from __future__ import division
from touEnv import AwakeHours, ApplianceLoad, ACLoadSavings, ACSeasons, AllAppliances, WeeksinMonth
from importSurvey import AppLogits_Saving, AppLogits_Ratio
import numpy as np
from random import shuffle, choice

#import survey logit for each appliance
'''Agent class representing house residents and their operations'''


class Resident:
    
    
    def __init__(self, ID, applianceList, applianceWeekday, weekdayHours, ToUInfluence, willUseAuto, \
                 willUseAutoTemp, ToUKnowledge, ToUKnowHow, socialNetwork, Children, offPeakAppliances, BillPayment, SavingsTip, RatioTip):
        
        self.ID = ID
        self.appliances = applianceList #Add something when survey is final, and define appliances owned by each respondent, with a string
        #self.load = Load
        self.ToUKnowledge = ToUKnowledge #if agent knows about ToU or not
        self.ToUKnowHow = ToUKnowHow #if agent knows how to estimate savings or not
        self.ToUInfluence = ToUInfluence #social influence
        self.paysBills = BillPayment #Variable based on agent paying for exact usage
        self.applianceWeekday = applianceWeekday #This is a dictionary where appliance: no. of times used weekdays #Maximum of 7
        self.weekdayHours = weekdayHours #This is a dictionary where appliance: preferred hours of usage, and weekend
        #self.amountSavedMonthly = 0 #This is updated each epoch based on the agent's appliance usage
        self.willUseAuto = willUseAuto
        self.willUseAutoTemp = willUseAutoTemp
        self.changeTemp = 0
        self.children = Children
        self.socialNetwork = socialNetwork
        self.offPeakAppliances = offPeakAppliances
        self.savingsTip = SavingsTip
        self.ratioTip = RatioTip
        
    def SocialNetworkInfluence(self, tempAgentList):
#         influence = 0
#         
#             return True
#         else:
#             return False
        
        if len(self.offPeakAppliances) > 0:
            curFriend =  np.random.choice(self.socialNetwork,1)[0] #returns ID of friend selected based on random uniform
            if len(tempAgentList[curFriend].offPeakAppliances) > 0 and self.ToUInfluence >3:
                self.offPeakAppliances = AllAppliances
#                 prob = self.ToUInfluence/6 
#                 influence = np.random.choice(np.arange(0,2), p = [1-prob, prob])
        
#         if influence ==1:
#             self.offPeakAppliances = AllAppliances
        
    
    def WillMoveAppliance(self, appliance, currentScenario):
        #Add code to simulate social network influence: if not shiftLoad, if randomly selected agent connection makes profit, agent would shift load
        #decision based on probability of how likely they are to change behaviour
        
        #First check if appliance in off peak appliances
        
        #Add appliance to offpeak appliances if theres a shift
        prediction = 0

        
        if self.ToUKnowHow:
            #return True
            #make decisions based on savings
            #applianceLogit = AppLogits_Ratio[appliance]
            
            savings_ = (currentScenario.currentToU.peakPrice - currentScenario.currentToU.offPeakPrice) * ApplianceLoad[appliance] * self.applianceWeekday[appliance] * WeeksinMonth
            
            #prediction = applianceLogit.predict(exog = dict(Savings = savings_, Children = self.Children))
            
            if self.savingsTip[appliance] is not None and self.savingsTip[appliance] <= savings_ :
                #print self.savingsTip[appliance], savings_
                if appliance not in self.offPeakAppliances:
                    self.offPeakAppliances.append(appliance)
                return True
            elif self.savingsTip[appliance] is None and appliance in self.offPeakAppliances:
                return True
            else:
                return False
                
        else:
            #make decisions based on ratio
            if appliance in self.offPeakAppliances:
                return True
    
            touRatio = currentScenario.currentToU.peakPrice/ currentScenario.currentToU.offPeakPrice
#             if appliance == 'Washing Machine':
#                 applianceLogit = AppLogits_Ratio[appliance]
#                 
#                 prediction = applianceLogit.predict(exog = dict(Ratio = touRatio, Children = self.children))
#             else:
            if self.ratioTip[appliance] <= touRatio and self.ratioTip[appliance] != 0:
                self.offPeakAppliances.append(appliance)
                return True
            else:
                return False
        
        
        if prediction >= 0.5:
            self.offPeakAppliances.append(appliance)
            return True
        else:
            return False                           
        
        
    def WillChangeTemp(self, savings, currentScenario):
        
        return 1
    
    def estimateSavings(self, weekLoad, currentScenario):
        
        #The first 5 days of the week load are weekdays and the last 2 days are weekends
        #Weekends are always assumed to be off-peak
        curSeason = currentScenario.currentSeason

        peakHours = np.hstack((np.tile(currentScenario.currentToU.PeakHours[curSeason],5), np.zeros(48)))
        midPeakHours = np.hstack((np.tile(currentScenario.currentToU.MidPeakHours[curSeason],5), np.zeros(48)))
        offPeakHours = np.hstack((np.tile(currentScenario.currentToU.OffPeakHours[curSeason],5), np.ones(48)))
        
        hourlyPayments = (peakHours * currentScenario.currentToU.peakPrice \
                          + midPeakHours * currentScenario.currentToU.midPeakPrice \
                          + offPeakHours * currentScenario.currentToU.offPeakPrice) * weekLoad
                        
        savings = -np.sum(hourlyPayments) /currentScenario.curWeeks * WeeksinMonth #4 weeks in a month
        return savings
            
    def ApplianceLoadChange(self, currentScenario):
        weekLoad = np.zeros(168)
        numWeeks = currentScenario.curWeeks
        if self.paysBills and len(self.appliances) > 0 and self.ToUKnowledge:
            #For each appliance, check if agent will move appliance
            curSeason = currentScenario.currentSeason
            loadShiftHours =  currentScenario.currentToU.OffPeakHours[curSeason]
            peakHours = currentScenario.currentToU.MidPeakHours[curSeason] + currentScenario.currentToU.PeakHours[curSeason] 

            weekLoad = np.zeros(168) #Number of Hours in a week, Last 2 days are the weekend
            
            for week in range(numWeeks):
                doneWashingDrying = False #To ensure we dont do washing-drying sequence twice
                
                for appliance in self.appliances:
                    #We'll put the washing machine load and cloth dryer in sequence
                    
                    #Ensure that in agent definition, washing machine comes before cloth dryer
                    
                    
                    shiftLoad = self.WillMoveAppliance(appliance, currentScenario)
    
                    
                    #Check if they dont already pay attention to ToU and shift this particular appliance as a result
                    #If they already shift loads, the shift is represented in the Ontario daily load, so we don't need to measure their shift
                    
    
                        
                    if shiftLoad or (currentScenario.auto and self.willUseAuto == 1):
#                         if appliance not in self.offPeakAppliances:
#                             self.offPeakAppliances.append(appliance)
                        #For cases where the agent owns both washing machine and dryer, we can simulate usage back-to-back
                        
                        if appliance in ['Washing Machine', 'Cloth Dryer'] and 'Washing Machine' in self.appliances\
                         and 'Cloth Dryer' in self.appliances and not doneWashingDrying: 
                            
                            weekdayCount = 0
                            weekDays = [0,1,2,3,4] #We only need week days since those current weekend appliance usages are already reflected in Ontario load
                            
                            if currentScenario.auto and self.willUseAuto:
                                allDays = np.arange(0,5)
                            else:
                                allDays = np.arange(0,7)
                            
                            while weekdayCount < self.applianceWeekday['Washing Machine']:
                                
                                
                                #First check if agent would use appliance during a peak period
                                potentialUseHours = np.where(self.weekdayHours['Washing Machine']>0)[0]
                                peakHours_ = np.where(peakHours>0)[0]
                                
                                currentDay = np.random.choice(allDays)
                                useHour  = np.random.choice(potentialUseHours) #This uses a uniform distribution
                                if useHour in peakHours_ or currentDay < 5:
                                    wouldUseDuringPeak =  True
                                else:
                                    wouldUseDuringPeak =  False
                                #Move appliance usage to off-peak hours
                                
                                
                                
                                if wouldUseDuringPeak:
#                                     if (currentScenario.auto and self.willUseAuto):
#                                         currentDay = np.random.choice(np.arange(0,5))
#                                     else:
#                                         currentDay = np.random.choice(allDays)
                                    if currentDay<5:
                                        #Move Load to a week day
                                        potentialHours = AwakeHours + loadShiftHours + self.weekdayHours['Washing Machine']
                                        hoursToShift = np.where(potentialHours > 2)[0]
                                        if not hoursToShift.size:
                                            potentialHours = AwakeHours + loadShiftHours
                                            hoursToShift = np.where(potentialHours > 1)[0]
                                            
                                        noWashDrySequence = True
                                            
                                        #Find 2 simultaneous time slots for washing and drying
                                        checkCount = 0 
                                        while noWashDrySequence and checkCount < hoursToShift.size:
                                            #This while loop might be problematic in certain cases.
                                            washHour = np.random.choice(hoursToShift)
                                            dryHour = washHour +1
                                            checkCount+=1
                                            if dryHour in hoursToShift and dryHour <24:
                                                noWashDrySequence = False
                                                
                                                
                                        #if len(weekDays) == 0:
                                            #weekDays = [0,1,2,3,4] #In cases where an appliance is used more than 5 times on weekdays
                                        #shuffle(weekDays)
                                        #currentDay = weekDays.pop()
                                        
                                        #Now add load to non peak hours
                                        weekLoad[currentDay *24 + washHour] += ApplianceLoad['Washing Machine']
                                        weekLoad[currentDay *24 + dryHour] += ApplianceLoad['Cloth Dryer']
                                        
        
                                    
                                    else: 
                                        #Simulate shift to weekend
                                        hoursToShift = np.where(AwakeHours > 0)[0]
                                        
                                        noWashDrySequence = True
                                            
                                        #Find 2 simultaneous time slots for washing and drying
                                        while noWashDrySequence:
                                            #This while loop might be problematic in certain cases.
                                            washHour = np.random.choice(hoursToShift)
                                            dryHour = washHour +1
                                            if dryHour in hoursToShift and dryHour <24:
                                                noWashDrySequence = False
                                                
                                        
                                        #Now add load to non peak hours
                                        weekLoad[currentDay *24 + washHour] += ApplianceLoad['Washing Machine']
                                        weekLoad[currentDay *24 + dryHour] += ApplianceLoad['Cloth Dryer']
                                    
                                    #Subtract load from peakHours
                                    if currentDay >4:
                                        currentDay = np.random.choice([0,1,2,3,4]) #We need a peak period on a week day to move the load
                                    peakLoadHours = np.where(peakHours>0)[0]
                                    noWashDrySequence = True
                                    while noWashDrySequence:
                                        washHour = np.random.choice(peakLoadHours)
                                        dryHour = washHour +1
                                        if dryHour in peakLoadHours and dryHour <24:
                                            noWashDrySequence = False
                                    
                                    weekLoad[currentDay *24 + washHour] -= ApplianceLoad['Washing Machine']
                                    weekLoad[currentDay *24 + dryHour] -= ApplianceLoad['Cloth Dryer']
                                    
                                    
                                weekdayCount+=1
                            doneWashingDrying = True
    
                        #Execute each appliance normally    
                        elif (appliance in ['Washing Machine', 'Cloth Dryer'] and not doneWashingDrying) or appliance is 'Dishwasher':
                            weekdayCount = 0
                            weekDays = [0,1,2,3,4]
                            
                            if appliance is 'Dishwasher':# or (currentScenario.auto and self.willUseAuto):
                                allDays = np.arange(0,5) #We only simulate weekday shifts for dishwashers; don't wanna keep those dirty dishes till the weekend :)
                            else:
                                allDays = np.arange(0,7)
                                
                            while weekdayCount < self.applianceWeekday[appliance]:
                                #First check if agent would use appliance during a peak period
                                potentialUseHours = np.where(self.weekdayHours[appliance]>0)[0]
                                peakHours_ = np.where(peakHours>0)[0]
                                
                                currentDay = np.random.choice(allDays)
                                useHour  = np.random.choice(potentialUseHours) #This uses a uniform distribution
                                if useHour in peakHours_ or currentDay < 5:
                                    wouldUseDuringPeak =  True
                                else:
                                    wouldUseDuringPeak =  False
                                
                                if wouldUseDuringPeak:
                                    #Move appliance usage to off-peak hours
                                    
                                    if currentDay<5:
                                        #Move Load to a week day
                                        potentialHours = AwakeHours + loadShiftHours + self.weekdayHours[appliance]
                                        hoursToShift = np.where(potentialHours > 2)[0]
                                        if not hoursToShift.size:
                                            potentialHours = AwakeHours + loadShiftHours
                                            hoursToShift = np.where(potentialHours > 1)[0]
                                            
                                        
                                        useHour = np.random.choice(hoursToShift)
                                                
                                                
                                        #if len(weekDays) == 0:
                                            #weekDays = [0,1,2,3,4] #In cases where an appliance is used more than 5 times on weekdays
                                        #shuffle(weekDays)
                                        #currentDay = weekDays.pop()
                                        
                                        #Now add load to non peak hours
                                        weekLoad[currentDay *24 + useHour] += ApplianceLoad[appliance]
        

                                    else: 
                                        #Simulate shift to weekend
                                        hoursToShift = np.where(AwakeHours > 0)[0]
                                        
                                        useHour = np.random.choice(hoursToShift)
                                               
                                        
                                        #Now add load to non peak hours
                                        weekLoad[currentDay *24 + useHour] += ApplianceLoad[appliance]
                                
                                    #Subtract load from peakHours
                                    if currentDay >4:
                                        currentDay = np.random.choice([0,1,2,3,4]) #We need a peak period on a week day to move the load
                                    peakLoadHours = np.where(peakHours>0)[0]
                                    prevUseHour = np.random.choice(peakLoadHours)
                                        
                                    weekLoad[currentDay *24 + prevUseHour] -= ApplianceLoad[appliance]
                                    
                                weekdayCount+=1
                                
    #                     elif appliance is 'Dishwasher':
    #                         #We only shift dishwasher usage on weekdays
    #                         weekdayCount = 0
    #                         weekDays = [0,1,2,3,4]
    #                         
    #                         
    #                         allDays = np.arange(0,5)
    #                         
    #                         while weekdayCount < self.applianceWeekday['Dishwasher']:
    #                             #Move appliance usage to off-peak hours
    #                             currentDay = np.random.choice(allDays)
    # 
    #                             potentialHours = AwakeHours + loadShiftHours + self.weekdayHours['Dishwasher']
    #                             hoursToShift = np.where(potentialHours > 2)[0]
    #                             if not hoursToShift.size:
    #                                 potentialHours = AwakeHours + loadShiftHours
    #                                 hoursToShift = np.where(potentialHours > 1)[0]
    #                                 
    #                             
    #                             useHour = np.random.choice(hoursToShift)
    #                                     
    #                                     
    #                             if len(weekDays) == 0:
    #                                 weekDays = [0,1,2,3,4] #In cases where an appliance is used more than 5 times on weekdays
    #                             shuffle(weekDays)
    #                             currentDay = weekDays.pop()
    #                             
    #                             #Now add load to non peak hours
    #                             weekLoad[currentDay *24 + useHour] += ApplianceLoad['Dishwasher']
    # 
    #                             
    #                             #Subtract load from peakHours
    #                             peakLoadHours = np.where(peakHours>0)[0]
    #                             prevUseHour = np.random.choice(peakLoadHours)
    #                                 
    #                             weekLoad[currentDay *24 + prevUseHour] -= ApplianceLoad[appliance]

        #self.amountSavedMonthly = self.estimateSavings(weekLoad, currentScenario)
        #print 'saved', self.amountSavedMonthly
        return weekLoad
    
    def SummerThermoSet(self, currentScenario):
        numWeeks = currentScenario.curWeeks
        if currentScenario.currentSeason in ACSeasons:
            dayLoad = np.zeros(24)
            dayLoad[9:16] -= ACLoadSavings
            #numWeeks = 26 #Number of weeks in a season
            
              
            weekLoad = np.hstack((np.tile(dayLoad,5), np.zeros(48))) * numWeeks
            savings = self.estimateSavings(weekLoad, currentScenario) #Monthly savings
            #print savings
            #Check if agent would willingly change temperature based on expected savings
            changeTemp = self.WillChangeTemp(savings, currentScenario)
            
            if changeTemp:
                self.changeTemp = 1
            
            if self.changeTemp or self.willUseAutoTemp:
                return weekLoad
            else:
                return np.zeros(168)
        else: 
            return np.zeros(168)