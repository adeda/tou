Agent-based simulation for using appliances in response to Time of Use electricity pricing. 
Version 1.0 

Appliances include washing machine, dishwasher, and clothes dryer.

Run simulation with applianceUsageSim.py. The agents have to be populated with a survey or modeler configurations.

Damola, damola.adepetu@gmail.com