from __future__ import division
import agents
import numpy as np
import simScenarios
import touSchemes
import matplotlib.pyplot as plt
import pickle

#First add up load data
with open('AllLoads2', 'rb') as f:
        AllLoads = pickle.load(f)
TotalLoad = np.zeros(8760)
count=0        
for load in AllLoads:
    TotalLoad += np.asarray(load)
plt.plot(TotalLoad[168*15:168*15+168*2])
plt.show()

plt.plot(TotalLoad[168*12:168*12+168*2])
plt.show()

def weeklyProfiles(load):
    count = 0
    weekProfile = np.zeros(52*24)
    while count < 52:
        curWeekLoad = load[count*168: (count+1)*168]
        for i in range(7):
            weekProfile[count*24: (count+1)*24] += curWeekLoad[i*24: (i+1)*24]
        count +=1
    
    return weekProfile/7 #Average daily load profile
# def getSeasonProfiles(dlperWeek, numWeeks, startWeek):
#     aveDaily = np.zeros(24)
#     if numWeeks + startWeek >51:
#         Loads = np.hstack((dlperWeek[startWeek:52],dlperWeek[0:numWeeks + startWeek-52]))
#     else:
#         Loads = dlperWeek[startWeek:startWeek+numWeeks]
#     return Loads
#         
    
    
weekLoad = weeklyProfiles(TotalLoad)
#dlprofile = getSeasonProfiles(weeLoad, 15, )

AgentList = []
applianceList = ['Cloth Dryer', 'Washing Machine', 'Dishwasher']
applianceWeekday = {'Dishwasher': 7, 'Washing Machine': 5, 'Cloth Dryer': 5}
appHr = np.asarray([0,0,0,0,0,0, 0,0,0,0,0,0, 1,1,1,1,1,0,  0,0,0,0,0,0])

weekdayHours = {'Dishwasher': appHr, 'Washing Machine': appHr, 'Cloth Dryer': appHr}
ToUInfluence = {'Dishwasher': 0, 'Washing Machine': 0, 'Cloth Dryer': 0}
AgentList.append(agents.Resident(1,applianceList, applianceWeekday, weekdayHours, ToUInfluence,1,1,1,1,[],1, ['Washing Machine']))



sumpeak =       np.asarray([0,0,0,0,0,0, 0,0,0,0,0,1, 1,1,1,1,1,0, 0,0,0,0,0,0])
summidpeak =    np.asarray([0,0,0,0,0,0, 0,1,1,1,1,0, 0,0,0,0,0,1, 1,0,0,0,0,0])
offpeak =       np.asarray([1,1,1,1,1,1, 1,0,0,0,0,0, 0,0,0,0,0,0, 0,1,1,1,1,1])
ToU_Seasons = {'peakHours': {'summer': sumpeak, 'fall': sumpeak,'winter': summidpeak,'spring':summidpeak}, \
               'midPeakHours': {'summer': summidpeak, 'fall': summidpeak,'winter': sumpeak,'spring':sumpeak}, \
               'offPeakHours': {'summer': offpeak, 'fall': offpeak,'winter': offpeak,'spring':offpeak}}


testToU = touSchemes.Scheme(.35,0.17,0.08, ToU_Seasons)
seasons = ['summer','fall','winter','spring']
seasonWeeks = [10,20,12,10]
startWeek = 1
testScen = simScenarios.Scenario(testToU, (np.arange(1,15,0.25)).tolist(), 1, 1, 'Test', seasons, seasonWeeks, startWeek, )
numWeeks = 13
seasonRounds = 20
for season in range(seasonRounds):
    for agent in AgentList:
        newLoad = agent.ApplianceLoadChange(testScen)
        #newLoad = agent.SummerThermoSet(testScen, numWeeks)
#         plt.bar(np.arange(1,169),newLoad)
#         plt.show()
    testScen.updateToU()
#print np.sum(np.abs(newLoad)/2)


        
    
    
    