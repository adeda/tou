from __future__ import division
import json
import numpy as np
import requests
import pandas as pd
from statsmodels.formula.api import logit
from statsmodels.api import MNLogit
import csv
from sklearn.linear_model import LassoLarsCV, LassoLars
import matplotlib.pyplot as plt
import math
from touEnv import AllAppliances
SurveyAgentKeys = ['Household Residents', 'School Age Children', 'Occupancy','Winter Bill','Summer Bill','Income',\
                   'Education', 'Appliances', 'Bill Payment', 'ToU Knowledge', 'Perceived Peak/Off-peak', \
                   'Perceived Savings', 'Off-peak Appliances', 'Washing Machine Savings', 'Cloth Dryer Savings',\
                   'Dishwasher Savings', 'Washing Machine Ratio', 'Cloth Dryer Ratio', 'Dishwasher Ratio', \
                   'Washing Machine Times', 'Cloth Dryer Times', 'Dishwasher Times','Washing Machine Frequency',\
                   'Cloth Dryer Frequency', 'Dishwasher Frequency' , 'Social Influence', 'Auto Appliance', 'Auto Temp',\
                   'Set Temp Savings']
def notNone(A):
    return A is not None

def collectSurvey(): 
    #Check responses that meet requirements:
    validIDs = []
    SurveyStats = {'Income': [], 'School Age Children': [], 'Does ToU':[], 'Knows ToU':[], 'Perceived Peak/Off-peak':[], 'Perceived Savings':[], 'Will Use Auto':[]}
    crowd_file = open("survey/f876782.csv", "rb")
    crowd_reader = csv.reader(crowd_file)
    for resp in crowd_reader:
        if resp[9] == 'ON':
            validIDs.append(int(resp[2]))
    print 'Ontario respondents:', len(validIDs)
    #Request json file
    
    #url ="https://api.crowdflower.com/v1/jobs/876782.json?/type=full&key=sM8ZimeB3FatgwyLzzVv" 
    
    url = "https://api.crowdflower.com/v1/jobs/876782/judgments.json?key=sM8ZimeB3FatgwyLzzVv"
    data = requests.get(url).json()
    
#     with open('survey/judgments.json') as data_file:    
#         data = json.load(data_file)
#         surveydata = data['results']['judgments']
    resp = data['888826064']
    numResponse = len(data['888826064']['_ids'])
    GoodResponses = []
    #TotalCount = len(surveydata)
    ValidResponses = 0
    FakeRes = 0
    
    for idx in range(numResponse):
        #Check that it is from Ontario, check test question, and cehck that they agree to participate
        #Add check based on ToU knowledge
        #resp = response['data']
        if resp['do_you_agree_with_the_following_statement_three_dollars_plus_ten_dollars_is_equal_to_five_dollars']['res'][idx] == 'disagree' and \
        resp['consent_to_participant_i_agree_of_my_own_free_will_to_participate_in_this_study']['res'][idx] == 'i_agree_to_participate' and resp['_ids'][idx] in validIDs:
            ValidResponses +=1
            
            #Now collect responses
            
            curagent = dict()
            
            #Household occupants
            #if 'how_many_people_live_in_your_household' in resp:
            if resp['how_many_people_live_in_your_household']['res'][idx] in ['1','2','3']:
                curagent['Household Residents'] = int(resp['how_many_people_live_in_your_household']['res'][idx])
            elif resp['how_many_people_live_in_your_household']['res'][idx] == 'more_than_3':
                curagent['Household Residents'] = 4
            else:
                curagent['Household Residents'] = None
                
            #School age children
           
            if resp['do_you_have_any_school_age_6__12_years_children_living_in_your_household']['res'][idx] == 'yes':
                curagent['School Age Children'] = 1
                SurveyStats['School Age Children'].append(1)
            else:
                curagent['School Age Children'] = 0
                SurveyStats['School Age Children'].append(0)
#             else:
#                 curagent['School Age Children'] = None
       
            #Weekday occupancy 7am -7pm
            
            if resp['do_you_or_anyone_in_your_household_spend_time_at_home_during_the_day_7am__7pm_on_weekdays_monday__friday']['res'][idx] == 'yes':
                curagent['Occupancy'] = 1
            elif resp['do_you_or_anyone_in_your_household_spend_time_at_home_during_the_day_7am__7pm_on_weekdays_monday__friday']['res'][idx] == 'no':
                curagent['Occupancy'] = 0
            else:
                curagent['Occupancy'] = None
                        
            #Winter bill
            if resp['on_average_how_much_is_your_monthly_electricity_bill_in_winter_november__april']['res'][idx] == 'less_than_50':
                curagent['Winter Bill'] = 1
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_winter_november__april']['res'][idx] == '50__99':
                curagent['Winter Bill'] = 2
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_winter_november__april']['res'][idx] == '100__149':
                curagent['Winter Bill'] = 3
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_winter_november__april']['res'][idx] == '150__199':
                curagent['Winter Bill'] = 4
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_winter_november__april']['res'][idx] == '200_upwards':
                curagent['Winter Bill'] = 5
            else:
                curagent['Winter Bill'] = None
                        
            #Summer bill
            if resp['on_average_how_much_is_your_monthly_electricity_bill_in_summer_may__october']['res'][idx] == 'less_than_50':
                curagent['Summer Bill'] = 1
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_summer_may__october']['res'][idx] == '50__99':
                curagent['Summer Bill'] = 2
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_summer_may__october']['res'][idx] == '100__149':
                curagent['Summer Bill'] = 3
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_summer_may__october']['res'][idx] == '150__199':
                curagent['Summer Bill'] = 4
            elif resp['on_average_how_much_is_your_monthly_electricity_bill_in_summer_may__october']['res'][idx] == '200_upwards':
                curagent['Summer Bill'] = 5
            else:
                curagent['Summer Bill'] = None
            
            
            #Annual income

            if resp['what_is_your_annual_income']['res'][idx] == '024999':
                curagent['Income'] = 1        
            elif resp['what_is_your_annual_income']['res'][idx] == '2500049999':
                curagent['Income'] = 2
            elif resp['what_is_your_annual_income']['res'][idx] == '5000074999':
                curagent['Income'] = 3
            elif resp['what_is_your_annual_income']['res'][idx] == '7500099999':
                curagent['Income'] = 4
            elif resp['what_is_your_annual_income']['res'][idx] == '100000124999':
                curagent['Income'] = 5
            elif resp['what_is_your_annual_income']['res'][idx] == '125000149999':
                curagent['Income'] = 6
            elif resp['what_is_your_annual_income']['res'][idx] == '150000174999':
                curagent['Income'] = 7
            elif resp['what_is_your_annual_income']['res'][idx] == '175000199999':
                curagent['Income'] = 8
            elif resp['what_is_your_annual_income']['res'][idx] == '200000_and_up':
                curagent['Income'] = 9
            else:
                curagent['Income'] = None
            if curagent['Income'] is not None:
                SurveyStats['Income'].append(curagent['Income'])
                
            #Education
            if resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'no_schooling_completed':
                curagent['Education'] = 0
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'primary_school':
                curagent['Education'] = 1
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'some_high_school_but_no_diploma':
                curagent['Education'] = 2
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'high_school_diploma_or_ged':
                curagent['Education'] = 3
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'some_college_but_no_degree':
                curagent['Education'] = 4
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == '2year_college_degree':
                curagent['Education'] = 5
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == '4year_college_degree':
                curagent['Education'] = 6
            elif resp['what_is_the_highest_level_of_school_that_you_have_completed']['res'][idx] == 'graduatelevel_degree':
                curagent['Education'] = 7
            else:
                curagent['Education'] = None
    
    
            #Appliances
            curagent['Appliances'] = []
            curapp  = resp['which_of_the_following_electric_appliances_do_you_have_in_your_household_check_all_that_apply']['res'][idx]
            if 'washing_machine_laundry' in curapp:
                curagent['Appliances'].append('Washing Machine')
            if 'cloth_dryer_electric' in curapp:
                curagent['Appliances'].append('Cloth Dryer')
            if 'dishwasher' in curapp:
                curagent['Appliances'].append('Dishwasher')
            

            
            #Electricity bill payment
            
                
                
            if resp['how_do_you_pay_for_your_electricity_usage']['res'][idx] in \
            ['i_pay_to_the_local_electricity_utility', 'i_pay_my_landlord_based_on_the_electricity_bill']:
                curagent['Bill Payment'] = True
            else:
                curagent['Bill Payment'] = False

                
                
            #ToU Knowledge
            if resp['did_you_know_that_in_ontario_weekday_electricity_prices_monday__friday_7am__7pm_are_more_expensive_than_both_weekend_prices_and_weekday_nightly_prices']['res'][idx]\
            == 'yes':
                curagent['ToU Knowledge'] = True
                SurveyStats['Knows ToU'].append(1)
            else:
                curagent['ToU Knowledge'] = False
                SurveyStats['Knows ToU'].append(0)

                
            #Knowledge of peak/offpeak ratio

            if resp['do_you_know_by_how_much_the_weekday_price_monday__friday_7am__7pm_is_more_expensive_than_the_nightweekend_price']['res'][idx] == 'no':
                curagent['Perceived Peak/Off-peak'] = None
            elif resp['do_you_know_by_how_much_the_weekday_price_monday__friday_7am__7pm_is_more_expensive_than_the_nightweekend_price']['res'][idx] == \
            'yes_day_price_is_15_times_as_expensive_as_night_price':
                curagent['Perceived Peak/Off-peak'] = 1.5
            elif resp['do_you_know_by_how_much_the_weekday_price_monday__friday_7am__7pm_is_more_expensive_than_the_nightweekend_price']['res'][idx] == \
            'yes_day_price_is_2_times_as_expensive_as_night_price':
                curagent['Perceived Peak/Off-peak'] = 2
            elif resp['do_you_know_by_how_much_the_weekday_price_monday__friday_7am__7pm_is_more_expensive_than_the_nightweekend_price']['res'][idx] == \
            'yes_day_price_is_3_times_as_expensive_as_night_price':
                curagent['Perceived Peak/Off-peak'] = 3
            elif resp['do_you_know_by_how_much_the_weekday_price_monday__friday_7am__7pm_is_more_expensive_than_the_nightweekend_price']['res'][idx] == \
            'yes_day_price_is_more_than_3_times_as_expensive_as_night_price':
                curagent['Perceived Peak/Off-peak'] = 4
            else:
                curagent['Perceived Peak/Off-peak'] = None
            if curagent['Perceived Peak/Off-peak'] is not None:
                SurveyStats['Perceived Peak/Off-peak'].append(curagent['Perceived Peak/Off-peak'])
            
            #Perceived monthly savings
            if resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            'less_than_10_per_month':
                curagent['Perceived Savings'] = 5
            elif resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            '10_per_month':
                curagent['Perceived Savings'] = 10
            elif resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            '20_per_month':
                curagent['Perceived Savings'] = 20
            elif resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            '30_per_month':
                curagent['Perceived Savings'] = 30
            elif resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            '40_per_month':
                curagent['Perceived Savings'] = 40
            elif resp['how_much_money_do_you_think_you_could_save_each_month_by_operating_appliances_during_periods_when_the_electricity_is_cheaper']['res'][idx] == \
            'more_than_40_per_month':
                curagent['Perceived Savings'] = 50
            else:
                curagent['Perceived Savings'] = None
            
            if curagent['Perceived Savings'] is not None:
                SurveyStats['Perceived Savings'].append(curagent['Perceived Savings']) 
                
            
            #Appliance usage during off-peak
            curagent['Off-peak Appliances'] = []
            curappoff = resp['which_of_the_following_appliances_do_you_try_to_use_during_the_cheaper_periods_ie_in_order_to_reduce_your_bill_if_electricity_pricing_does_not_affect_your_appliance_usage_choose_none']['res'][idx]
            if 'washing_machine' in curappoff:
                curagent['Off-peak Appliances'].append('Washing Machine')
            if 'cloth_dryer' in curappoff:
                curagent['Off-peak Appliances'].append('Cloth Dryer')
            if 'dishwasher' in curappoff:
                curagent['Off-peak Appliances'].append('Dishwasher')

                        
            if len(curagent['Off-peak Appliances'])>0:
                SurveyStats['Does ToU'].append(1)
            else:
                SurveyStats['Does ToU'].append(0)
                   
            #Savings that'd disrupt washing machine usage
            if 'Washing Machine' in curagent['Appliances']:
                if idx > 452:
                    question = 'how_much_monthly_savings_from_your_washing_machine_would_urge_you_to_use_it_at_night_or_during_weekends'
                else:
                    question = 'how_much_savings_in_your_monthly_bill_would_urge_you_to_use_your_washing_machine_at_night_or_during_weekends'
                #print idx, resp[question]['res'][idx]   
                if resp[question]['res'][idx] == '10_per_month':
                    curagent['Washing Machine Savings'] = 10
                elif resp[question]['res'][idx] == '20_per_month':
                    curagent['Washing Machine Savings'] = 20
                elif resp[question]['res'][idx] == '30_per_month':
                    curagent['Washing Machine Savings'] = 25#30
                elif resp[question]['res'][idx] == '40_per_month':
                    curagent['Washing Machine Savings'] = 25#40
                elif resp[question]['res'][idx] == 'more_than_40_per_month':
                    curagent['Washing Machine Savings'] = 25#50
                elif resp[question]['res'][idx] == '5_per_month':
                    curagent['Washing Machine Savings'] = 5
                elif resp[question]['res'][idx] == '15_per_month':
                    curagent['Washing Machine Savings'] = 15
                elif resp[question]['res'][idx] == 'more_than_20_per_month':
                    curagent['Washing Machine Savings'] = 25#30
                else:
                    curagent['Washing Machine Savings'] = None
            else:
                curagent['Washing Machine Savings'] = None
                
            #Savings that'd disrupt cloth dryer usage       
            if 'Cloth Dryer' in curagent['Appliances']:
                if idx > 452:
                    question = 'how_much_monthly_savings_from_your_cloth_dryer_would_urge_you_to_use_it_at_night_or_during_weekends'
                else:
                    question = 'how_much_savings_in_your_monthly_bill_would_urge_you_to_use_your_cloth_dryer_at_night_or_during_weekends'
                    
                if resp[question]['res'][idx] == '10_per_month':
                    curagent['Cloth Dryer Savings'] = 10
                elif resp[question]['res'][idx] == '20_per_month':
                    curagent['Cloth Dryer Savings'] = 20
                elif resp[question]['res'][idx] == '30_per_month':
                    curagent['Cloth Dryer Savings'] = 25#30
                elif resp[question]['res'][idx]== '40_per_month':
                    curagent['Cloth Dryer Savings'] = 25#40
                elif resp[question]['res'][idx] == 'more_than_40_per_month':
                    curagent['Cloth Dryer Savings'] = 25#50
                elif resp[question]['res'][idx] == '5_per_month':
                    curagent['Cloth Dryer Savings'] = 5
                elif resp[question]['res'][idx] == '15_per_month':
                    curagent['Cloth Dryer Savings'] = 15
                elif resp[question]['res'][idx] == 'more_than_20_per_month':
                    curagent['Cloth Dryer Savings'] = 25#30
                else:
                    curagent['Cloth Dryer Savings'] = None
            
            else:
                curagent['Cloth Dryer Savings'] = None
                    
            #Savings that'd disrupt dishwasher usage       
            if 'Dishwasher' in curagent['Appliances']:
                if idx > 452:
                    question = 'how_much_monthly_savings_from_your_dishwasher_would_urge_you_to_use_it_at_night_or_during_weekends'
                else:
                    question = 'how_much_savings_in_your_monthly_bill_would_urge_you_to_use_your_dishwasher_at_night_or_during_weekends'
                    
                if resp[question]['res'][idx] == '10_per_month':
                    curagent['Dishwasher Savings'] = 10
                elif resp[question]['res'][idx] == '20_per_month':
                    curagent['Dishwasher Savings'] = 20
                elif resp[question]['res'][idx] == '30_per_month':
                    curagent['Dishwasher Savings'] = 25#30
                elif resp[question]['res'][idx] == '40_per_month':
                    curagent['Dishwasher Savings'] = 25#40
                elif resp[question]['res'][idx] == 'more_than_40_per_month':
                    curagent['Dishwasher Savings'] = 25#50
                elif resp[question]['res'][idx] == '5_per_month':
                    curagent['Dishwasher Savings'] = 5
                elif resp[question]['res'][idx] == '15_per_month':
                    curagent['Dishwasher Savings'] = 15
                elif resp[question]['res'][idx] == 'more_than_20_per_month':
                    curagent['Dishwasher Savings'] = 25#30
                else:
                    curagent['Dishwasher Savings'] = None
            else:
                curagent['Dishwasher Savings'] = None
                
                
            #Peak-Offpeak ratio that'd change washing machine usage
            if 'Washing Machine' in curagent['Appliances']:
                if resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_washing_machine']['res'][idx] == 'day_price_is_15_times_as_expensive_as_night_price':
                    curagent['Washing Machine Ratio'] = 1.5
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_washing_machine']['res'][idx] == 'day_price_is_2_times_as_expensive_as_night_price':
                    curagent['Washing Machine Ratio'] = 2
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_washing_machine']['res'][idx] == 'day_price_is_3_times_as_expensive_as_night_price':
                    curagent['Washing Machine Ratio'] = 3
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_washing_machine']['res'][idx] == 'day_price_is_more_than_3_times_as_expensive_as_night_price':
                    curagent['Washing Machine Ratio'] = 4
                else:
                    curagent['Washing Machine Ratio'] = 0
            
            else:
                curagent['Washing Machine Ratio'] = 0
                
            #Peak-Offpeak ratio that'd change cloth dryer usage
            if 'Cloth Dryer' in curagent['Appliances']:
                if resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_cloth_dryer']['res'][idx] == 'day_price_is_15_times_as_expensive_as_night_price':
                    curagent['Cloth Dryer Ratio'] = 1.5
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_cloth_dryer']['res'][idx] == 'day_price_is_2_times_as_expensive_as_night_price':
                    curagent['Cloth Dryer Ratio'] = 2
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_cloth_dryer']['res'][idx] == 'day_price_is_3_times_as_expensive_as_night_price':
                    curagent['Cloth Dryer Ratio'] = 3
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_cloth_dryer']['res'][idx] == 'day_price_is_more_than_3_times_as_expensive_as_night_price':
                    curagent['Cloth Dryer Ratio'] = 4
                else:
                    curagent['Cloth Dryer Ratio'] = 0
                
            else:
                curagent['Cloth Dryer Ratio'] = 0
                
            #Peak-Offpeak ratio that'd change dishwasher usage
            if 'Dishwasher' in curagent['Appliances']:
                if resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_dishwasher']['res'][idx] == 'day_price_is_15_times_as_expensive_as_night_price':
                    curagent['Dishwasher Ratio'] = 1.5
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_dishwasher']['res'][idx] == 'day_price_is_2_times_as_expensive_as_night_price':
                    curagent['Dishwasher Ratio'] = 2
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_dishwasher']['res'][idx] == 'day_price_is_3_times_as_expensive_as_night_price':
                    curagent['Dishwasher Ratio'] = 3
                elif resp['what_difference_between_the_day_price_and_night_price_would_urge_you_to_change_how_you_use_your_dishwasher']['res'][idx] == 'day_price_is_more_than_3_times_as_expensive_as_night_price':
                    curagent['Dishwasher Ratio'] = 4    
                else:
                    curagent['Dishwasher Ratio'] = 0
            else:
                curagent['Dishwasher Ratio'] = 0
                
            #Times of washing machine usage
            curagent['Washing Machine Times'] = np.zeros(24)
            if 'Washing Machine' in curagent['Appliances'] and notNone(resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]):
                if '6_am__9_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][6:9]+=1
                if '9_am__12_noon' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][9:12]+=1
                if '12_noon__3_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][12:15]+=1
                if '3_pm__6_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][15:18]+=1
                if '6_pm__9_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][18:21]+=1
                if '9_pm__6_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx]:
                    curagent['Washing Machine Times'][21:24]+=1
                    curagent['Washing Machine Times'][0:6]+=1
            
            #Times of cloth dryer usage
            curagent['Cloth Dryer Times'] = np.zeros(24)      
            if 'Cloth Dryer' in curagent['Appliances'] and notNone(resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]):
                if '6_am__9_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][6:9]+=1
                if '9_am__12_noon' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][9:12]+=1
                if '12_noon__3_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][12:15]+=1
                if '3_pm__6_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][15:18]+=1
                if '6_pm__9_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][18:21]+=1
                if '9_pm__6_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx]:
                    curagent['Cloth Dryer Times'][21:24]+=1
                    curagent['Cloth Dryer Times'][0:6]+=1
                    
            #Times of dishwasher usage
            curagent['Dishwasher Times'] = np.zeros(24)
            if 'Dishwasher' in curagent['Appliances'] and notNone(resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]):
                if '6_am__9_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][6:9]+=1
                if '9_am__12_noon' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][9:12]+=1
                if '12_noon__3_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][12:15]+=1
                if '3_pm__6_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][15:18]+=1
                if '6_pm__9_pm' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][18:21]+=1
                if '9_pm__6_am' in resp['during_which_of_the_following_time_periods_of_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx]:
                    curagent['Dishwasher Times'][21:24]+=1
                    curagent['Dishwasher Times'][0:6]+=1
                    
            #Frequency of washing machine usage
            if 'Washing Machine' in curagent['Appliances']:
                if resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == 'once_per_week':
                    curagent['Washing Machine Frequency'] = 1
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == '2_times_per_week':
                    curagent['Washing Machine Frequency'] = 2
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == '3_times_per_week':
                    curagent['Washing Machine Frequency'] = 3
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == '4_times_per_week':
                    curagent['Washing Machine Frequency'] = 4
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == '5_times_per_week_':
                    curagent['Washing Machine Frequency'] = 5
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_washing_machine']['res'][idx] == 'more_than_5_times_per_week':
                    curagent['Washing Machine Frequency'] = 6
                else:
                    curagent['Washing Machine Frequency'] = 0
            else:
                curagent['Washing Machine Frequency'] = 0
                
            #Frequency of cloth dryer usage
            if 'Cloth Dryer' in curagent['Appliances']:
                if resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == 'once_per_week':
                    curagent['Cloth Dryer Frequency'] = 1
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == '2_times_per_week':
                    curagent['Cloth Dryer Frequency'] = 2
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == '3_times_per_week':
                    curagent['Cloth Dryer Frequency'] = 3
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == '4_times_per_week':
                    curagent['Cloth Dryer Frequency'] = 4
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == '5_times_per_week':
                    curagent['Cloth Dryer Frequency'] = 5
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_cloth_dryer']['res'][idx] == 'more_than_5_times_per_week':
                    curagent['Cloth Dryer Frequency'] = 6
                else:
                    curagent['Cloth Dryer Frequency'] = 0
            else:
                curagent['Cloth Dryer Frequency'] = 0
                
            #Frequency of dishwasher usage
            if 'Dishwasher' in curagent['Appliances']:
                if resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == 'once_per_week':
                    curagent['Dishwasher Frequency'] = 1
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == '2_times_per_week':
                    curagent['Dishwasher Frequency'] = 2
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == '3_times_per_week':
                    curagent['Dishwasher Frequency'] = 3
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == '4_times_per_week':
                    curagent['Dishwasher Frequency'] = 4
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == '5_times_per_week':
                    curagent['Dishwasher Frequency'] = 5
                elif resp['how_many_times_on_weekdays_monday__friday_do_you_typically_use_your_dishwasher']['res'][idx] == 'more_than_5_times_per_week':
                    curagent['Dishwasher Frequency'] = 6
                else:
                    curagent['Dishwasher Frequency'] =0
            else:
                curagent['Dishwasher Frequency'] =0
                    
            #Social influence
            
            if resp['if_your_friends_or_close_relatives_tell_you_that_they_have_saved_some_money_by_changing_how_they_use_appliances_how_likely_is_this_to_change_your_appliance_usage']['res'][idx]\
            == 'very_likely':
                curagent['Social Influence'] = 5
            
            elif resp['if_your_friends_or_close_relatives_tell_you_that_they_have_saved_some_money_by_changing_how_they_use_appliances_how_likely_is_this_to_change_your_appliance_usage']['res'][idx]\
            == 'somewhat_likely':
                curagent['Social Influence'] = 4
                
            elif resp['if_your_friends_or_close_relatives_tell_you_that_they_have_saved_some_money_by_changing_how_they_use_appliances_how_likely_is_this_to_change_your_appliance_usage']['res'][idx]\
            == 'neutral':
                curagent['Social Influence'] = 3
                
            elif resp['if_your_friends_or_close_relatives_tell_you_that_they_have_saved_some_money_by_changing_how_they_use_appliances_how_likely_is_this_to_change_your_appliance_usage']['res'][idx]\
            == 'somewhat_unlikely':
                curagent['Social Influence'] = 2
                
            elif resp['if_your_friends_or_close_relatives_tell_you_that_they_have_saved_some_money_by_changing_how_they_use_appliances_how_likely_is_this_to_change_your_appliance_usage']['res'][idx]\
            == 'very_unlikely':
                curagent['Social Influence'] = 1
            else:
                curagent['Social Influence'] = None
                
            #Appliance Smart Device

            if resp['if_youre_given_a_smart_device_to_automatically_schedule_your_appliances_dishwasher_washing_machine_cloth_dryer_to_lower_your_bill_would_you_use_it']['res'][idx] == 'yes':
                curagent['Auto Appliance'] = 1
            elif resp['if_youre_given_a_smart_device_to_automatically_schedule_your_appliances_dishwasher_washing_machine_cloth_dryer_to_lower_your_bill_would_you_use_it']['res'][idx] == 'maybe':
                curagent['Auto Appliance'] = 0.5
            elif resp['if_youre_given_a_smart_device_to_automatically_schedule_your_appliances_dishwasher_washing_machine_cloth_dryer_to_lower_your_bill_would_you_use_it']['res'][idx] == 'no':
                curagent['Auto Appliance'] = 0
            else:
                curagent['Auto Appliance'] = None
            if  curagent['Auto Appliance'] is not None:
                SurveyStats['Will Use Auto'].append(curagent['Auto Appliance'])
            
            #Temperature Smart Device
            if resp['if_youre_given_a_smart_device_to_automatically_control_your_home_temperature_would_you_use_it']['res'][idx] == 'yes':
                curagent['Auto Temp'] = 1
            elif resp['if_youre_given_a_smart_device_to_automatically_control_your_home_temperature_would_you_use_it']['res'][idx] == 'maybe':
                curagent['Auto Temp'] = 0.5
            elif resp['if_youre_given_a_smart_device_to_automatically_control_your_home_temperature_would_you_use_it']['res'][idx] == 'no':
                curagent['Auto Temp'] = 0
            else:
                curagent['Auto Temp'] = None
                

            #Set temperature savings
            if resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == 'none':
                curagent['Set Temp Savings'] = None
            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '$10_per_month':
                curagent['Set Temp Savings'] = 10
            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '20_per_month':
                curagent['Set Temp Savings'] = 20
            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '30_per_month':
                curagent['Set Temp Savings'] = 30
            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '40_per_month':
                curagent['Set Temp Savings'] = 40
                
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '5_per_month':
                curagent['Set Temp Savings'] =5
                
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == '15_per_month':
                curagent['Set Temp Savings'] =15
            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == 'more_than_20_per_month':
                curagent['Set Temp Savings'] = 25
                            
            elif resp['how_much_monthly_savings_in_summer_would_make_you_set_your_home_temperature_at_25_77f_during_the_day']['res'][idx] == 'more_than_40_per_month':
                curagent['Set Temp Savings'] = 50
            else:
                curagent['Set Temp Savings'] = None
            
            if curagent['Perceived Peak/Off-peak'] and curagent['ToU Knowledge'] == 0:
                FakeRes+=1
            else:    
                GoodResponses.append(curagent)
        
            
            FakeRes+=1
#     #print FakeRes
#     plt.hist(SurveyStats['Income'])
#     plt.xlabel('Income Class')
#     plt.ylabel('Number of Respondents')
#     plt.title('Income')
#     plt.show()
#       
#     plt.hist(SurveyStats['School Age Children'])
#     plt.title('School Age Children')
#     plt.xlabel('Presence of Children')
#     plt.ylabel('Number of Respondents')
#     plt.show()
    
#     print SurveyStats['Does ToU'].count(0), SurveyStats['Does ToU'].count(1)     
#     plt.hist(SurveyStats['Does ToU'])
#     plt.title('Does ToU')
#     plt.show()
#     
#     plt.ylabel('Number of Respondents')
#     plt.show()
#       
#     plt.hist(SurveyStats['Knows ToU'])
#     plt.title('Knows ToU')
# 
#     plt.ylabel('Number of Respondents')
#     plt.show()
#       
#     print SurveyStats['Perceived Peak/Off-peak'].count(1.5), SurveyStats['Perceived Peak/Off-peak'].count(2), SurveyStats['Perceived Peak/Off-peak'].count(3), SurveyStats['Perceived Peak/Off-peak'].count(4)
#     plt.hist(SurveyStats['Perceived Peak/Off-peak'])#, bins=range(min(SurveyStats['Perceived Peak/Off-peak']), max(SurveyStats['Perceived Peak/Off-peak']) + 1, 1))
#     plt.title('Perceived Peak/Off-peak')
#     plt.xlabel('Peak/Off-peak Ratio')
#     plt.ylabel('Number of Respondents')
#     plt.show()
#     print SurveyStats['Perceived Savings'].count(5), SurveyStats['Perceived Savings'].count(10), len(SurveyStats['Perceived Savings'])    
#     plt.hist(SurveyStats['Perceived Savings'], bins=range(min(SurveyStats['Perceived Savings']), max(SurveyStats['Perceived Savings']) + 5, 5))
#     plt.title('Perceived Savings')
#     plt.xlabel('Savings')
#     plt.ylabel('Number of Respondents')
#     plt.show()
#   
    print SurveyStats['Will Use Auto'].count(0), SurveyStats['Will Use Auto'].count(0.5), SurveyStats['Will Use Auto'].count(1)     
#     plt.hist(SurveyStats['Will Use Auto'])
#     plt.title('Will Use Auto')
# 
#     plt.ylabel('Number of Respondents')
#     plt.show()
    
    #plt.hist(data, bins=range(min(data), max(data) + binwidth, binwidth))
    
    
    return GoodResponses

surveyResponses = collectSurvey()
# for res in surveyResponses:
#     if ['Dishwasher Ratio'] <=2 and len(res['Off-peak Appliances']) == 0:
#         print 'who this'
# for resp in surveyResponses:
#     print resp['Income']

# for k in SurveyAgentKeys:
#     if k not in['Washing Machine Times', 'Cloth Dryer Times', 'Dishwasher Times']:
#         val = []
#         for agent in surveyResponses:
#              
#             if k in agent:
#                 if agent[k] is not None:
#                     val.append(agent[k])
#     print k, len(val)
    


def noShiftPoint(val, vtype):
    downval = []
    upval = []
    if vtype == 'ratio':
        if val ==4:
            #downval = [1.5,2,3]
            downval = [3]
            
        elif val == 2:
            downval = [1.5]
            #upval = [3,4]
        elif val == 3:
            downval = [2]
            #upval = [4]
#         elif val == 1.5:
#             
#             upval= [2,3,4]

    elif vtype == 'savings':
        if val ==50:
            downval = [40]
            
        elif val == 40:
            downval = [30]
            #upval = [50]
        elif val == 30:
            downval = [20]
            #upval = [40,50]
        elif val == 20:
            downval = [10]
            #upval = [30,40,50]
        elif val == 25:
            downval = [20]
            
        elif val == 15:
            downval = [10]
            #upval = [20, 25]
        
        elif val == 10:
            #upval = [20,30,40,50]
            downval = [5]
#         elif val ==5:
#             upval = [10,15, 20,25]
    return downval, upval
            

#print len(surveyResponses)

def selectFeatures(appliance, vartype):
    Indep = []
    agCount = 0
    if vartype == 'ratio':
        perception = 'Perceived Peak/Off-peak'
        stated = appliance + ' Ratio'
    elif vartype == 'savings':
        perception = 'Perceived Savings'
        stated = appliance + ' Savings'
       
    if appliance in AllAppliances:
        for agent in surveyResponses:
            if notNone(agent['Household Residents']) and notNone(agent['School Age Children'])  \
             and notNone(agent['Summer Bill']) and notNone(agent['Winter Bill'])  and notNone(agent['Income']) and notNone(agent['Education']):
                agCount+=1
                #First use historical decision patterns
                divider = 1
                if agent['Bill Payment'] and notNone(agent[perception]) and agent[perception]!=0 and notNone(agent['Off-peak Appliances']): #and \ notNone(agent['Perceived Savings'])
                    if agent['ToU Knowledge'] == 1:
                        if len(agent['Off-peak Appliances']) > 0:
                            decision = 1.0
                            if vartype == 'savings':
                                divider = len(agent['Off-peak Appliances'])
                        else:
                            decision = 0.0
                            divider =1
                        
        #             Indep.append([decision, agent['Household Residents'], agent['School Age Children'], agent['Income'], agent['Perceived Peak/Off-peak']])
                        Indep.append([decision, agent['Household Residents'], agent['School Age Children'],  agent['Summer Bill'], agent['Winter Bill']\
                                , agent['Income'], agent['Education'], agent[perception]/divider])
                    #Indep.append(dec_)
                    
        #         if notNone(agent['Washing Machine Savings']) and notNone(agent['Washing Machine Ratio']) and 'washing_machine_laundry' in agent['Appliances']:
                if appliance in agent['Appliances']:
                    if agent[stated]!=0 and agent[stated] is not None:
                        #print agent[stated]
                        Indep.append([1.0, agent['Household Residents'], agent['School Age Children'],  agent['Summer Bill'], agent['Winter Bill']\
                                , agent['Income'], agent['Education'], agent[stated]])
        #                 Indep.append([1, agent['Household Residents'], agent['School Age Children'], agent['Income'], agent['Washing Machine Ratio']])
                        
                        downVar, upVar = noShiftPoint(agent[stated], vartype)
                        #noShiftSavings = noShiftPoint(agent['Washing Machine Savings'],'savings')
                        
                        if downVar:
                            for dvar in downVar:
                                Indep.append([0.0, agent['Household Residents'], agent['School Age Children'], agent['Summer Bill'], agent['Winter Bill']\
                                    , agent['Income'], agent['Education'], dvar])
        #                         Indep.append([0, agent['Household Residents'], agent['School Age Children'], agent['Income'], ratio])
                        if upVar:
                            for uvar in upVar:
                                Indep.append([1.0, agent['Household Residents'], agent['School Age Children'],  agent['Summer Bill'], agent['Winter Bill']\
                                    , agent['Income'], agent['Education'], uvar])
        #                         Indep.append([1, agent['Household Residents'], agent['School Age Children'],  agent['Income'],  ratio])
        
        print 'Number of observations:', len(Indep),'Agents Used', agCount
                        
    elif appliance == 'Temperature':
        for agent in surveyResponses:
            if notNone(agent['Household Residents']) and notNone(agent['School Age Children'])  \
             and notNone(agent['Summer Bill']) and notNone(agent['Winter Bill'])  and notNone(agent['Income'])  and notNone(agent['Education']) and notNone(agent['Set Temp Savings']) :
                agCount+=1


                Indep.append([1.0, agent['Household Residents'], agent['School Age Children'],  agent['Summer Bill'], agent['Winter Bill']\
                        , agent['Income'], agent['Education'], agent['Set Temp Savings']])
#                 Indep.append([1, agent['Household Residents'], agent['School Age Children'], agent['Income'], agent['Washing Machine Ratio']])
                
                downVar, upVar = noShiftPoint(agent['Set Temp Savings'], vartype)
                #noShiftSavings = noShiftPoint(agent['Washing Machine Savings'],'savings')
                
                if downVar:
                    for dvar in downVar:
                        Indep.append([0.0, agent['Household Residents'], agent['School Age Children'], agent['Summer Bill'], agent['Winter Bill']\
                            , agent['Income'], agent['Education'], dvar])
#                         Indep.append([0, agent['Household Residents'], agent['School Age Children'], agent['Income'], ratio])
                if upVar:
                    for uvar in upVar:
                        Indep.append([1.0, agent['Household Residents'], agent['School Age Children'],  agent['Summer Bill'], agent['Winter Bill']\
                            , agent['Income'], agent['Education'], uvar])
#                         Indep.append([1, agent['Household Residents'], agent['School Age Children'],  agent['Income'],  ratio])
    
    else: #Social network logit
        for agent in surveyResponses:
            if notNone(agent['Household Residents']) and notNone(agent['School Age Children'])  \
             and notNone(agent['Summer Bill']) and notNone(agent['Winter Bill'])  and notNone(agent['Income'])  and notNone(agent['Education']) and notNone(agent['Social Influence']) :
                agCount+=1


                Indep.append([float(agent['Social Influence']), agent['Household Residents'], agent['School Age Children'], agent['Income'], agent['Education'],  agent['Summer Bill'], agent['Winter Bill']\
                        ])
      
        
        print 'Number of observations:', len(Indep),'Agents Used', agCount
    return Indep
  
AppLogits_Saving = dict()
AppLogits_Ratio = dict()
def getLogit(app, var_type, formula_, setCols):

    Indep_ = selectFeatures(app, var_type.lower())
    IndepArray = np.array(Indep_)

    pd.options.display.float_format = '${:,.2f}'.format
    if setCols:
        #SurveyFrame = pd.DataFrame(IndepArray, columns= setCols)
        appliance_logit_ = MNLogit(IndepArray[:,0], IndepArray[:,1:5])
        #appliance_logit = MNLogit.from_formula('Decision ~ '+  formula_, data=SurveyFrame)
        appliance_logit = appliance_logit_.fit()
        #print appliance_logit.summary()
        numVars =4#len(setCols)-3
    else:
        SurveyFrame = pd.DataFrame(IndepArray, columns=['Decision', 'Residents','Children', 'SummerBill', 'WinterBill','Income','Education',var_type])
    # SurveyFrame = pd.DataFrame(IndepArray, columns=['Decision', 'Residents','Children', 'Income','Ratio'])
        appliance_logit = logit(formula = 'Decision ~ '+ var_type+ formula_, data=SurveyFrame).fit()
        numVars = 7
    print appliance_logit.summary()
#     if var_type == 'Ratio':
#         AppLogits_Ratio[app] = appliance_logit
#     else:
#         AppLogits_Saving[app] = appliance_logit
        
    lasso_model = LassoLarsCV(cv=7, normalize = True).fit(IndepArray[:,1:], IndepArray[:,0])
    #lasso_model = LassoLars(normalize = True, alpha = 0.001).fit(IndepArray[:,1:], IndepArray[:,0])
       
    plt.bar(np.arange(numVars), lasso_model.coef_)
    plt.xticks(np.arange(numVars)+0.5, ['Residents','Children', 'SummerBill', 'WinterBill','Income','Education',var_type],size = 15)
    plt.ylabel('Coefficient', size = 20)
    plt.xlabel('Features', size=20)
    if app == 'Cloth Dryer':
        label_ = 'Clothes Dryer'
    else:
        label_ = app
    plt.title('Feature Selection for using '+ label_ + '; Focus: '+ var_type, size = 20)
    plt.show()
    return appliance_logit

#SocialMNLogit =    getLogit('Social', 'Savings', '+ SummerBill + Residents + Children + WinterBill + Income + Education', ['Decision','Residents','Children', 'Income','Education'])
#AppLogits_Saving['Temperature']= getLogit('Temperature', 'Savings', '+ SummerBill', 0)
 
# AppLogits_Ratio['Washing Machine']= getLogit('Washing Machine', 'Ratio', '+ Residents + Children + SummerBill + WinterBill + Income + Education', 0)
#AppLogits_Ratio['Cloth Dryer']= getLogit('Cloth Dryer', 'Ratio', '+ Residents + Children + SummerBill + WinterBill + Income + Education', 0)
#AppLogits_Ratio['Dishwasher']= getLogit('Dishwasher', 'Ratio', '+ Education', 0)
#  
#  
# AppLogits_Saving['Washing Machine']= getLogit('Washing Machine', 'Savings', '+ SummerBill + WinterBill + Education', 0)
AppLogits_Saving['Cloth Dryer']= getLogit('Cloth Dryer', 'Savings', '+ SummerBill + WinterBill + Education', 0)
# AppLogits_Saving['Dishwasher']= getLogit('Dishwasher', 'Savings', '+ WinterBill', 0)


def checkResiduals(logit__, NumBins):
    #Check residuals
    idx = np.argsort(np.asarray(logit__.fittedvalues))
    argGen = np.asarray(logit__.resid_generalized)[idx]
    sortedFit = np.sort(np.asarray(logit__.fittedvalues))
    minFit = sortedFit[0]
    maxFit = sortedFit[-1]
    print minFit, maxFit
    # plt.plot(sortedFit)
    # plt.show()
    #NumBins = 40
    binStep = (maxFit-minFit)/NumBins
    
    Fitted = np.zeros(NumBins+1)
    General = np.zeros(NumBins+1)
    BinCount = np.zeros(NumBins+1)
    
    for idx, fit  in enumerate(sortedFit):
       
        curbin = int(math.floor((fit - minFit)/binStep))
        Fitted[curbin]+= fit
        General[curbin] += argGen[idx]
        BinCount[curbin] +=1
    
    ConfInts = 2* np.sqrt(.95*0.5/BinCount)
    
    #plt.scatter(logit__.fittedvalues, logit__.resid_generalized)
    plt.scatter(Fitted/BinCount, General/BinCount, s = 25)
    plt.plot(Fitted/BinCount, ConfInts, linestyle = '--',label = 'Upper 95% Confidence Interval')
    plt.plot(Fitted/BinCount, -ConfInts, linestyle = '--',label = 'Lower 95% Confidence Interval')
    plt.scatter(Fitted/BinCount, ConfInts,color = 'r', alpha = 0.5)
    plt.scatter(Fitted/BinCount, -ConfInts,color ='r', alpha = 0.5)
    # plt.plot(np.zeros(2)-0.137)
    plt.xlabel('Average Fitted Values')
    plt.ylabel('Average Residuals')
    plt.legend()
    plt.show()

#checkResiduals(appliance_logit, 20)