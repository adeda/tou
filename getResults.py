from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
import pickle
from touEnv import TotalSimRuns, SimYears, NumAgents
import pylab as ppl
import math


Scenarios = ['Base Case', '4 Seasons', '2 Seasons', 'Information Campaign', 'Auto Appliances', 'Ratio of 4:1', '2 Seasons with Auto Appliances']
SeasonNumberScenarios_1 = ['Base Case', '4 Seasons']
SeasonNumberScenarios_2 = ['Base Case', '2 Seasons']
SeasonAuto = ['Base Case', 'Auto Appliances']
SeasonRatio = ['Base Case', 'Ratio of 4:1']
SeasonInfo= ['Base Case', 'Information Campaign']
SeasonAuto2 = ['Base Case', '2 Seasons with Auto Appliances']

ScenarioCases = {'Base Case':'Base Case','4 Seasons': 'Opt4', '2 Seasons':'Opt2', 'Information Campaign':'Info', 'Auto Appliances':'Auto', 'Ratio of 4:1': 'Peak4', '2 Seasons with Auto Appliances': 'Opt2Auto'}
AllRes = pickle.load(open('OntarioToU_Final','rb'))
PTA = {'Base Case': None, '4 Seasons': None, '2 Seasons': None, 'Information Campaign':None\
       , 'Auto Appliances': None, 'Ratio of 4:1': None}

StartDates = {'Base Case': 18, '4 Seasons': 11, '2 Seasons': 16, 'Information Campaign':18\
       , 'Auto Appliances': 18, 'Ratio of 4:1': 18, '2 Seasons with Auto Appliances': 16}

tval = 1.987

lines = [':','-','-.','--']
colors = ['k','b','r','g','c']

#Collect load data from file
with open('AllLoads2', 'rb') as f:
    AllLoads = pickle.load(f)       
 
TotalLoad = np.zeros(8760)
count=0        
for load in AllLoads:
    TotalLoad += np.asarray(load)
    count+=1

TotalLoad *=NumAgents/100

def peakToAverageRatio(load):
    peakload = max(load)
    average = np.mean(load)
    return peakload/average

def getSeasonProfile(load, startweek, numweeks):
    
    
    if startweek + numweeks > 52:
        seasonLoad = np.hstack((load[startweek*168:52*168], load[0:168*(startweek + numweeks - 52)]))
    else:
        seasonLoad = load[startweek*168: (startweek + numweeks)*168]
    
    
    weekDayProfiles = np.zeros(24)
    weekendProfiles = np.zeros(24)
    
    for i in range(numweeks):
        #for j in range(5):
        
        #First day of Ontario 2015 load is thursday
        weekDayProfiles += seasonLoad[(i*7)*24: (i*7+1)*24]
        weekDayProfiles += seasonLoad[(i*7+1)*24: (i*7+2)*24]
        weekDayProfiles += seasonLoad[(i*7+4)*24: (i*7+5)*24]
        weekDayProfiles += seasonLoad[(i*7+5)*24: (i*7+6)*24]
        weekDayProfiles += seasonLoad[(i*7+6)*24: (i*7+7)*24]
        
        #for j in range(2):
        weekendProfiles += seasonLoad[(i*7+2)*24: (i*7+3)*24]
        weekendProfiles += seasonLoad[(i*7+3)*24: (i*7+4)*24]
    
    weekDayProfiles /= (5*numweeks)
    weekendProfiles /= (2*numweeks)
    
    return weekDayProfiles.astype(int)

def estimateLoadProfile(scenario):
     
    
    if scenario == '4 Seasons':
        SeasonLoadProfiles = {'summer': None, 'fall': None, 'winter': None, 'spring': None}
        SeasonMean = {'summer': None, 'fall': None, 'winter': None, 'spring': None}
        SeasonErr = {'summer': None, 'fall': None, 'winter': None, 'spring': None}
    else:
        SeasonLoadProfiles = {'summer': None, 'winter': None}
        SeasonMean = {'summer': None, 'winter': None}
        SeasonErr = {'summer': None, 'winter': None}
    BCLoadDelta = None
    ScenarioLoadDelta = None
#     for res in AllRes:
#         if res['Scenario'] == 'Base Case':
#              
#             curProf =  res['Results']['Load Delta']
# 
#             if BCLoadDelta is not None:
#                 BCLoadDelta = np.column_stack((curProf, BCLoadDelta))
#             else:
#                 BCLoadDelta = curProf
#     
#     for res in AllRes:
#         if res['Scenario'] == scenario:
#              
#             curProf =  res['Results']['Load Delta']
# 
#             if ScenarioLoadDelta is not None:
#                 ScenarioLoadDelta = np.column_stack((curProf, ScenarioLoadDelta))
#             else:
#                 ScenarioLoadDelta = curProf
#     
#     dateDiff = StartDates[scenario] - StartDates['Base Case']
#     if dateDiff > 0:
#         LoadDiff = ScenarioLoadDelta - np.column_stack((BCLoadDelta[: , dateDiff*168: ] , BCLoadDelta[: , 0:dateDiff*168 ]))
#     elif dateDiff < 0:
#         LoadDiff = ScenarioLoadDelta - np.column_stack((BCLoadDelta[: , (52+dateDiff)*168: ] , BCLoadDelta[: , 0:(52+dateDiff)*168 ]))
#     else:
#         LoadDiff = ScenarioLoadDelta - BCLoadDelta
#         
#       
#     #Get Load Profiles for each scenario
#     if scenario in ['2 Seasons with Auto Appliances', '2 Seasons']:
#         SeasonWeeks = [26,26]
#     elif scenario == '4 Seasons':
#         SeasonWeeks = [10,17,7,18]
        
    
    
    for res in AllRes:
        if res['Scenario'] == scenario:
            curProf =  res['Results']['Weekday Load Profile']
 
            for season, loadprofile in curProf.iteritems():
                if SeasonLoadProfiles[season] is not None:
                    SeasonLoadProfiles[season] = np.column_stack((SeasonLoadProfiles[season], loadprofile))
                else:
                    SeasonLoadProfiles[season] = loadprofile #Convert to MWh
    
    #Adjust dates
    for season, loadprofiles in SeasonLoadProfiles.iteritems():
        #print loadprofiles
        SeasonMean[season] = np.mean(loadprofiles/NumAgents, axis =1)
        SeasonErr[season] = np.std(loadprofiles/NumAgents, axis =1)*tval/math.sqrt(TotalSimRuns)
    
    return SeasonMean, SeasonErr




def estimateAnnualPTA(scenario):
     
    scenPTA = [np.zeros(SimYears)]*TotalSimRuns
        
    for res in AllRes:
        if res['Scenario'] == scenario:
            curPTA_ =  res['Results']['PTA']
            for pta in curPTA_:
                #print  scenPTA[res['SimRun']]
                scenPTA[res['SimRun']][pta['Sim Year']] += pta['PTA'] *  pta['Weeks']
    
    for idx, pta in enumerate(scenPTA):
        if idx == 0:
            finalPTA = pta
        else:
            finalPTA = np.column_stack((finalPTA, pta))
     
    finalPTA /=52 #Results already multiplied based on week length of each season, this gives the annual average
    meanRes = np.mean(finalPTA, axis =1)
    errRes = np.std(finalPTA, axis =1)*tval/math.sqrt(TotalSimRuns)
    
    return meanRes, errRes
XTick = np.linspace(2016,2021, SimYears).tolist()
# for scen in Scenarios:
#     meanVar, errVar = estimateAnnualPTA(scen)
#     ppl.errorbar(np.arange(0, SimYears), meanVar,errVar, label = scen , linewidth = 3,  alpha = 0.8, elinewidth = 2)
# 
# ppl.legend(loc=2, numpoints = 1, fontsize = 35)
# ppl.xlabel('Time (6-month Epochs)', size = 40)
# ppl.ylabel('Agents with PV Systems', size = 40)
# ppl.xticks(np.arange(0,SimYears), XTick, size = 30)
# ppl.yticks(size = 30)
# ppl.title('Peak ', size = 50)
# ppl.show()
DayTicks = ['Midnight - 1AM', '','','','','','6AM - 7AM', '','','','','',\
            'Noon - 1PM', '','','','','','6PM - 7PM', '','','','','11PM - Midnight']
count = 0
colorcount = 0
for scen in SeasonInfo:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label = ScenarioCases[scen] + ': '+season.title() , linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show()

count = 0
colorcount = 0
for scen in SeasonNumberScenarios_1:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        curPTA = peakToAverageRatio(mv)
        print scen, season, curPTA
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label = ScenarioCases[scen] + ': '+season.title(), linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show()
     

count = 0
colorcount = 0
for scen in SeasonNumberScenarios_2:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        curPTA = peakToAverageRatio(mv)
        print scen, season, curPTA
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label =  ScenarioCases[scen] + ': '+season.title() , linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show()     

count = 0
colorcount = 0
for scen in SeasonAuto:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        curPTA = peakToAverageRatio(mv)
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label = ScenarioCases[scen] + ': '+season.title() , linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show() 

count = 0
colorcount = 0
for scen in SeasonAuto2:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        curPTA = peakToAverageRatio(mv)
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label = ScenarioCases[scen] + ': '+season.title() , linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show()     

count = 0
colorcount = 0
for scen in SeasonRatio:
    meanVar, errVar = estimateLoadProfile(scen)

        
    for season, mv in meanVar.iteritems():
        curPTA = peakToAverageRatio(mv)
        ppl.errorbar(np.arange(0, 24), mv,errVar[season], label =  ScenarioCases[scen] + ': '+season.title() , linewidth = 3,  linestyle = lines[count], alpha = 0.8, elinewidth = 2)
        count+=1
        colorcount+=1
        if count ==4:
            count = 0
ppl.legend(loc=2, numpoints = 1,fontsize = 25)
ppl.xlabel('Time of Day', size = 40)
ppl.ylabel('Average Household Load (kWh)', size = 40)
ppl.xticks(np.arange(24),DayTicks,size = 30)
ppl.yticks(size = 30)
ppl.title('Weekday Load Profiles', size = 50)
ppl.show()
