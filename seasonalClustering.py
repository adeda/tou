from __future__ import division
import numpy as np
import pickle
import matplotlib.pyplot as plt
#First add up load data
with open('AllLoads2', 'rb') as f:
        AllLoads = pickle.load(f)
TotalLoad = np.zeros(8760)
count=0        
for load in AllLoads:
    TotalLoad += np.asarray(load)
    count+=1
# plt.plot(TotalLoad)
# plt.show()
# print count

minWeeks= 4
maxWeeks = 40
def createSeasons(numSeasons, subSeason):
    '''Subseason is the number of weeks left to complete a year'''
    
    if numSeasons == 2:
        AllSeasons = []
        for s in range(minWeeks,min(subSeason,maxWeeks)):
            curS = [0]*2
            if subSeason-s <= min(subSeason,maxWeeks) and subSeason-s>=minWeeks:
                curS[0] = s
                curS[1] = subSeason-s
                if [curS[1], curS[0]] not in AllSeasons:
                    AllSeasons.append(curS)
        return AllSeasons
        
    else:
        newS = []
        for s in range(minWeeks,maxWeeks):
            if (subSeason -s) >=minWeeks: 
                getS = createSeasons(numSeasons-1, subSeason -s)
                
                for seasons in getS:
                    count= 0
                    fit = True
                    while count <numSeasons:
                        seasons = seasons[1:] + seasons[0:1]
                        count+=1
                        if seasons in newS:
                            fit = False
                            break
                    if fit:
                        newS.append([s]+seasons)
        return newS



def Euclidean(A,B):
    dist = (A-B)**2
    dist = np.sum(dist)
    dist = np.sqrt(dist)
    
    return dist

def calcR2(seasons):
    distCenter = 0
    selfDist = 0
    
    for idx, s in enumerate(seasons):
        
        if idx:
            AllSeasons = np.vstack((s, AllSeasons))
        else:
            AllSeasons = s
    totalCenter = np.mean(AllSeasons,axis=0)
    for idx, s in enumerate(seasons):
        center = np.mean(s,axis=0)
        for subs in s:
            selfDist += (Euclidean(center,subs))**2
            distCenter += (Euclidean(totalCenter,subs))**2
    #print   1-selfDist/distCenter
    return 1-selfDist/distCenter

def featurize(load):
    percentile_75 = np.percentile(load,75)
    percentile_50 = np.percentile(load,50)
    feats = []
    for l in load:
        if l >= percentile_75:
            feats.append(1)
        elif l < percentile_75 and l >= percentile_50:
            feats.append(0.5)
        else:
            feats.append(0)
    return np.asarray(feats)
#Convert load to features
yeardays = 364

 
for d in range(yeardays):
    if d == 0:
        loadfeat = featurize(TotalLoad[d*24:(d+1)*24])
    else:
        loadfeat = np.hstack((loadfeat, featurize(TotalLoad[d*24:(d+1)*24])))
# plt.plot(loadfeat)
# plt.show()
BestSeasons = ([[21, 31], 44], [[10, 24, 18], 10], [[9, 18, 12, 13], 35], [[8, 13, 9, 18, 4], 14], [[4, 6, 5, 10, 9, 18], 10], [[4, 6, 5, 10, 9, 8, 10], 10])
def plotSeasons(seasons):
    
    for idx,s in enumerate(seasons):
        seasonArray = np.zeros(52)
        #numSeasons = len(s[0])
        startWeek = s[1]
        seasonArray[startWeek] = 1
        for week in s[0]:
            startWeek += week
            if startWeek >51:
                startWeek -= 52 
            seasonArray[startWeek] =1
        seasonArray = np.cumsum(seasonArray)
        seasonArray[np.where(seasonArray == 0)] = len(s[0])
        if idx:
            finalSeasons = np.vstack((finalSeasons, seasonArray))
        else:
            finalSeasons = seasonArray
    return finalSeasons
         
def getFeat(startweek,endweek):
    feats= np.zeros(168)
    if startweek < endweek:
        weekdiff = endweek - startweek
        for week in range(weekdiff):
            if week == 0:
                feats = loadfeat[(startweek+week)*168: (startweek+week+1)*168]
            else:
                feats = np.vstack((feats, loadfeat[(startweek+week)*168: (startweek+week+1)*168]))
    else:
        
        for week in (range(startweek, 52) + range(0,endweek)):
            if feats.any():
                feats = np.vstack((feats, loadfeat[(week)*168: (week+1)*168]))
            else:
                feats = loadfeat[(week)*168: (week+1)*168]
    
    return feats
         
#Now create feature space using seasons:
allr2 = [0.21, 0.28, 0.35, 0.39, 0.42, 0.45]
# for s in range(2,8):
#     newSeasons = createSeasons(s,52)
#     highestR2 = 0
#     selectSeason = 0 
#     for season in newSeasons:
#         
#     
#         for startpoint in range(52):
#             AllSeasons = []
#             total = 0 + startpoint
#             for s in season:
#                 if s+ total>=52:
#                     curfeat = getFeat(total, total+s-52)
#                 else:
#                     curfeat = getFeat(total, total+s)
#                 total+=s
#                 if total >=52:
#                     total -=52
#                     
#                 AllSeasons.append(curfeat)
#                 
#             curR2 = calcR2(AllSeasons)
#             
#             if highestR2 <curR2:
#                 highestR2 = curR2
#                 selectSeason = [season, startpoint]
#     allr2.append(highestR2)
#     print highestR2 , selectSeason


# xaxis = np.arange(2,8)
# plt.plot(xaxis, allr2, linestyle = '--',color='r')
# plt.scatter(xaxis, allr2)
# plt.xlabel('Number of Seasons',size= 17)
# plt.ylabel('R Squared',size= 17)
# plt.title('Seasonality: New Results',size= 20)
# #plt.xticks(np.arange(0,6), xaxis)
# plt.show()
            
      
            
seasonArr = plotSeasons(BestSeasons)
plt.imshow(seasonArr, interpolation ='none', cmap=plt.cm.coolwarm_r)
plt.grid(which='all', axis='both', linestyle='-')
plt.ylabel('Number of Seasons', size= 15)
plt.xlabel('Week of the Year', size= 15)
plt.yticks(np.arange(0,6), np.arange(2,8), size = 13)
plt.xticks(np.arange(0,52), np.arange(1,53), size = 13)
plt.show()
            
            
            
            
    
            
    
            
        
        

        