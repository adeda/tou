from __future__ import division
import numpy as np
from touEnv import OffPeakElecPrice, ACSeasons, SimYears, HoursinWeek, AwakeHours, NumHouseholdsinON, TotalSimRuns
from importSurvey import surveyResponses
import math
import random 
import agents
import simScenarios
import copy
import touSchemes
import matplotlib.pyplot as plt
import pickle
import csv

def runSim(ResidentList, CurrentScenario):
    simPeriod = 0
    firstSim = True
    #ScaledLoad = TotalLoad * len(ResidentList)/100 #100 load traces summed up
    peak2Ave = []
    Results = dict()
    finalSimYearLoadDelta = dict()
    
    #Implement info campaign scenario
    if CurrentScenario.infoCampaign:
        for res in ResidentList:
            res.ToUKnowledge =  True
            res.ToUKnowHow = True

    while simPeriod < SimYears:

                

        
        for season in CurrentScenario.seasonOrder:
            totalLoadDelta = np.zeros(HoursinWeek)
            for res in ResidentList:
                #For each agent, update social network influence
                
                #For each agent Estimate appliance usage (kWh) during current season
                appLoadDelta = res.ApplianceLoadChange(CurrentScenario)
                totalLoadDelta += appLoadDelta
                
#                 if CurrentScenario.currentSeason in ACSeasons:
#                     tempContDelta = res.SummerThermoSet(CurrentScenario)
#                     totalLoadDelta += tempContDelta
            
                
            #For each agent, estimate temperature savings if season is in ACSeasons
              
            #UpdateScenario
            CurrentScenario.updateToU()

                
            if firstSim:
                LoadDelta = totalLoadDelta
                firstSim = False
            else:
                LoadDelta = np.hstack((LoadDelta, totalLoadDelta))
            
            weekdayDelta = np.zeros(24)
            weekendDelta = np.zeros(24)
            
            for i in range(5):
                weekdayDelta += totalLoadDelta[24*i:24*(i+1)]
            for i in range(2):
                weekendDelta += totalLoadDelta[24*(i+5):24*(i+6)]
            
#             #Scale load delta to ON population
#             weekdayDelta *= NumHouseholdsinON /len(ResidentList)
#             weekendDelta *= NumHouseholdsinON /len(ResidentList)
            #Get season loads
            weekdayLoad, weekendLoad = getSeasonProfile(TotalLoad_, CurrentScenario.startWeek, CurrentScenario.curWeeks)
#             plt.plot(weekdayLoad + weekdayDelta/CurrentScenario.curWeeks/5, linestyle = '--')
#             plt.plot(weekdayLoad)
#             plt.show()
#             
#             plt.plot(weekendLoad + weekendDelta/CurrentScenario.curWeeks/2, linestyle = '--')
#             plt.plot(weekendLoad)
#             plt.show()
            
            PTA = peakToAverageRatio(weekdayLoad)
            peak2Ave.append({'PTA': PTA, 'Weeks': CurrentScenario.curWeeks, 'Season': CurrentScenario.currentSeason, 'Sim Year': simPeriod})
            #print 'Peak to Average:', PTA
            if SimYears - simPeriod ==1:
                #This is the final year of the simulation
                finalSimYearLoadDelta[CurrentScenario.currentSeason] = weekdayLoad + weekdayDelta/CurrentScenario.curWeeks/5
        
#             tempAgents = copy.deepcopy(ResidentList)
#             for res in ResidentList:
#                 res.SocialNetworkInfluence(tempAgents)    
        simPeriod +=1
#     plt.bar(np.arange(0,168*len(CurrentScenario.seasonOrder)*SimYears), LoadDelta)
#     plt.show()
    Results['Load Delta'] = LoadDelta
    Results['PTA'] = peak2Ave
    Results['Weekday Load Profile'] = finalSimYearLoadDelta
    return Results

def initializeAgents(replicas):
    ID=0
    numResp = len(surveyResponses)
    print 'Respondents:',numResp
    AllIDs= np.arange(0, numResp *replicas)
    AgentList = []
#     ToUknows = 0
    for i in range(numResp):
        k=0
        while k < replicas:
            surveyVars = surveyResponses[i]
#             if surveyVars['ToU Knowledge']:
#                 ToUknows+=1
            PotentialFriends = np.select([AllIDs!=ID], [AllIDs])
            numFriends = int(math.ceil(random.betavariate(1,3) *15))

            socNet = random.sample(PotentialFriends, numFriends)
            ToUKnowHow = False 
            applianceWeekday_ = dict()
            applianceHours = dict()
            for appliance in surveyVars['Appliances']:
                
                if surveyVars[appliance + ' Frequency'] ==0:
                    #Assume weekly usage based on uniform random function
                    applianceWeekday_[appliance] = random.randint(1,6)
                else:
                    applianceWeekday_[appliance] = surveyVars[appliance + ' Frequency']
                    
                if np.count_nonzero(surveyVars[appliance + ' Times']) == 0:
                    applianceHours[appliance] = AwakeHours
                
                else:
                    applianceHours[appliance] = surveyVars[appliance + ' Times']
            #applianceUsage = {'Dishwasher': surveyVars['Dishwasher Frequency'], 'Washing Machine': surveyVars['Washing Machine Frequency'], 'Cloth Dryer': surveyVars['Cloth Dryer Frequency']}
            #applianceHours = {'Dishwasher': surveyVars['Dishwasher Times'], 'Washing Machine': surveyVars['Washing Machine Times'], 'Cloth Dryer': surveyVars['Cloth Dryer Times']}
            if not surveyVars['ToU Knowledge']:
                offPeakApp = []
            else:
                offPeakApp = surveyVars['Off-peak Appliances']
            
            SavingsTip = {'Dishwasher': surveyVars['Dishwasher Savings'], 'Washing Machine': surveyVars['Washing Machine Savings'], 'Cloth Dryer': surveyVars['Cloth Dryer Savings']} #Savings that would make the agent change appliance usage
            RatioTip = {'Dishwasher': surveyVars['Dishwasher Ratio'], 'Washing Machine': surveyVars['Washing Machine Ratio'], 'Cloth Dryer': surveyVars['Cloth Dryer Ratio']}
            #Create Agent from survey response
            curagent = agents.Resident(ID, surveyVars['Appliances'], applianceWeekday_, applianceHours, surveyVars['Social Influence'],\
                                       surveyVars['Auto Appliance'], surveyVars['Auto Temp'], surveyVars['ToU Knowledge'], ToUKnowHow, \
                                       socNet, surveyVars['School Age Children'], offPeakApp, surveyVars['Bill Payment'], SavingsTip, RatioTip)
                                       

            
            
            k+=1
            ID+=1
        
            AgentList.append(curagent)
    #print 'Knows about tou:', ToUknows
    print 'numAgents:', len(AgentList)
    return AgentList
        

        
 #Collect load data from file
with open('AllLoads2', 'rb') as f:
    AllLoads = pickle.load(f)       
 
TotalLoad = np.zeros(8760)
count=0        
for load in AllLoads:
    TotalLoad += np.asarray(load)
    count+=1
# plt.plot(TotalLoad)
# plt.show()
# print count       
              
def getSeasonProfile(load, startweek, numweeks):
    
    
    if startweek + numweeks > 52:
        seasonLoad = np.hstack((load[startweek*168:52*168], load[0:168*(startweek + numweeks - 52)]))
    else:
        seasonLoad = load[startweek*168: (startweek + numweeks)*168]
    
    
    weekDayProfiles = np.zeros(24)
    weekendProfiles = np.zeros(24)
    
    for i in range(numweeks):
        #for j in range(5):
        
        #First day of Ontario 2015 load is thursday
        weekDayProfiles += seasonLoad[(i*7)*24: (i*7+1)*24]
        weekDayProfiles += seasonLoad[(i*7+1)*24: (i*7+2)*24]
        weekDayProfiles += seasonLoad[(i*7+4)*24: (i*7+5)*24]
        weekDayProfiles += seasonLoad[(i*7+5)*24: (i*7+6)*24]
        weekDayProfiles += seasonLoad[(i*7+6)*24: (i*7+7)*24]
        
        #for j in range(2):
        weekendProfiles += seasonLoad[(i*7+2)*24: (i*7+3)*24]
        weekendProfiles += seasonLoad[(i*7+3)*24: (i*7+4)*24]
    
    weekDayProfiles /= (5*numweeks)
    weekendProfiles /= (2*numweeks)
    
    return weekDayProfiles.astype(int), weekendProfiles.astype(int)

def peakToAverageRatio(load):
    peakload = max(load)
    average = np.mean(load)
    
    return peakload/average
# testWeekday, testWeekend = getSeasonProfile(TotalLoad,40, 26)
# plt.plot(testWeekday,color = 'r')
# plt.plot(testWeekend,color = 'b')
# plt.show()   
# ToUPolicies = []

ONload_file = open("HourlyDemands_2002-2015.csv", "rb")
ONload_reader = csv.reader(ONload_file)

ONHourlyLoad = []
for idx, hr in enumerate(ONload_reader):
    if idx >= 111073:
        #start load collection
        ONHourlyLoad.append(int(hr[3]) * 1000) #Convert from MWh to kWh
        if idx >= 111073 + 8735:
            break




sumpeak =       np.asarray([0,0,0,0,0,0, 0,0,0,0,0,1, 1,1,1,1,1,0, 0,0,0,0,0,0])
summidpeak =    np.asarray([0,0,0,0,0,0, 0,1,1,1,1,0, 0,0,0,0,0,1, 1,0,0,0,0,0])
offpeak =       np.asarray([1,1,1,1,1,1, 1,0,0,0,0,0, 0,0,0,0,0,0, 0,1,1,1,1,1])

#New 2 season scheme has same summer peak hours as existing one
summermidpeak_2season =  np.asarray([0,0,0,0,0,0, 0,0,0,1,1,0, 0,0,0,0,0,1, 1,1,1,0,0,0])
summeroffpeak_2season =  np.asarray([1,1,1,1,1,1, 1,1,1,0,0,0, 0,0,0,0,0,0, 0,0,0,1,1,1])

winterpeak_2season =     np.asarray([0,0,0,0,0,0, 0,0,0,0,0,0, 0,0,0,0,1,1, 1,1,1,0,0,0])
wintermidpeak_2season =  np.asarray([0,0,0,0,0,0, 0,0,1,1,1,1, 1,1,1,1,0,0, 0,0,0,0,0,0])
winteroffpeak_2season =  np.asarray([1,1,1,1,1,1, 1,1,0,0,0,0, 0,0,0,0,0,0, 0,0,0,1,1,1])

#Next for 4 season schemes
winterpeak_4season =     np.asarray([0,0,0,0,0,0, 0,0,0,0,0,0, 0,0,0,0,1,1, 1,1,1,1,0,0])
wintermidpeak_4season =  np.asarray([0,0,0,0,0,0, 0,0,1,1,1,1, 1,1,1,1,0,0, 0,0,0,0,0,0])
winteroffpeak_4season =  np.asarray([1,1,1,1,1,1, 1,1,0,0,0,0, 0,0,0,0,0,0, 0,0,0,0,1,1])

fallpeak_4season =     np.asarray([0,0,0,0,0,0, 0,0,0,0,0,0, 0,0,0,1,1,1, 1,1,0,0,0,0])
fallmidpeak_4season =  np.asarray([0,0,0,0,0,0, 0,0,1,1,1,1, 1,1,1,0,0,0, 0,0,0,0,0,0])
falloffpeak_4season =  np.asarray([1,1,1,1,1,1, 1,1,0,0,0,0, 0,0,0,0,0,0, 0,0,0,0,1,1])

springpeak_4season =     np.asarray([0,0,0,0,0,0, 0,0,1,1,1,1, 0,0,0,0,0,0, 0,1,1,0,0,0])
springmidpeak_4season =  np.asarray([0,0,0,0,0,0, 0,0,0,0,0,0, 1,1,1,1,1,1, 1,0,0,0,0,0])
springoffpeak_4season =  np.asarray([1,1,1,1,1,1, 1,1,0,0,0,0, 0,0,0,0,0,0, 0,0,0,1,1,1])

summerpeak_4season =     np.asarray([0,0,0,0,0,0, 0,0,0,0,0,1, 1,1,1,1,1,0, 0,0,0,0,0,0,])
summermidpeak_4season =  np.asarray([0,0,0,0,0,0, 0,0,0,1,1,0, 0,0,0,0,0,1, 1,1,1,0,0,0])
summeroffpeak_4season =  np.asarray([1,1,1,1,1,1, 1,1,1,0,0,0, 0,0,0,0,0,0, 0,0,0,1,1,1])


ToU_4Seasons = {'peakHours': {'summer': summerpeak_4season, 'fall': fallpeak_4season,'winter': winterpeak_4season,'spring': springpeak_4season}, \
               'midPeakHours': {'summer': summermidpeak_4season, 'fall': fallmidpeak_4season,'winter': wintermidpeak_4season,'spring': springmidpeak_4season}, \
               'offPeakHours': {'summer': summeroffpeak_4season, 'fall': falloffpeak_4season,'winter': winteroffpeak_4season,'spring': springoffpeak_4season}}
          
ToU_2Seasons = {'peakHours': {'summer': sumpeak, 'winter': winterpeak_2season}, \
               'midPeakHours': {'summer': summermidpeak_2season, 'winter': wintermidpeak_2season}, \
               'offPeakHours': {'summer': summeroffpeak_2season, 'winter': winteroffpeak_2season}}

ToU_BaseCase = {'peakHours': {'summer': sumpeak, 'winter': summidpeak}, \
               'midPeakHours': {'summer': summidpeak, 'winter': sumpeak}, \
               'offPeakHours': {'summer': offpeak, 'winter': offpeak}}
          
BaseCaseUpdater = np.linspace(1,1.5,num = SimYears*2+1).tolist()
SeasonUpdater_4 = np.linspace(1,1.5,num = SimYears*4+1).tolist()

SimRun = 0
numReplicas = 1 #repetition of agents
ONHourlyLoad_ = np.asarray(ONHourlyLoad)
finalRes = []
while SimRun < TotalSimRuns:
    ResList = initializeAgents(numReplicas)
    TotalLoad_ = TotalLoad * len(ResList)/100
    ScenVars_ = [{'name': 'Base Case', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': False, 'Auto Temp': False, \
              'ToU': ToU_BaseCase, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 18, 'Info Campaign': False,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087},
                 
                 {'name': '4 Seasons', 'updater': copy.deepcopy(SeasonUpdater_4), 'Auto Appliances': False, 'Auto Temp': False, \
              'ToU': ToU_4Seasons, 'Seasons': ['spring','summer','winter','fall'], 'Season Weeks': [10,17,7,18], 'Start Week': 11, 'Info Campaign': False,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087}, 
                 
                 
                 {'name': '2 Seasons', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': False, 'Auto Temp': False, \
              'ToU': ToU_2Seasons, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 16, 'Info Campaign': False,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087}, 
                 
                 
                {'name': '2 Seasons with Auto Appliances', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': True, 'Auto Temp': False, \
              'ToU': ToU_2Seasons, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 16, 'Info Campaign': False,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087}, 
                 
                 {'name': 'Information Campaign', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': False, 'Auto Temp': False, \
              'ToU': ToU_BaseCase, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 18, 'Info Campaign': True,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087},
                 
                 {'name': 'Auto Appliances', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': True, 'Auto Temp': False, \
              'ToU': ToU_BaseCase, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 18, 'Info Campaign': False,\
              'Peak Price': 0.18, 'Midpeak Price': 0.132, 'Offpeak Price': 0.087},
                 
                 {'name': 'Ratio of 4:1', 'updater': copy.deepcopy(BaseCaseUpdater), 'Auto Appliances': False, 'Auto Temp': False, \
              'ToU': ToU_BaseCase, 'Seasons': ['summer','winter'], 'Season Weeks': [26,26], 'Start Week': 18, 'Info Campaign': False,\
              'Peak Price': 0.26, 'Midpeak Price': 0.132, 'Offpeak Price': 0.065}
             
             ]
    
    for sv in ScenVars_:
        
        infcount = 0
        for res in ResList:
            if len(res.offPeakAppliances)>0:
                infcount+=1
        #print 'ToU Count:', infcount
        
        global CurrentScen_
        toU_ = touSchemes.Scheme(sv['Peak Price'], sv['Midpeak Price'], sv['Offpeak Price'], sv['ToU'])
        CurrentScen_ = simScenarios.Scenario(toU_, sv['updater'], sv['Auto Appliances'], sv['Auto Temp'], sv['name'], sv['Seasons'],\
                                   sv['Season Weeks'], sv['Start Week'], sv['Info Campaign'])
        #print CurrentScen_.Updater
        #print CurrentScen_.currentToU.offPeakPrice
        results = runSim(copy.deepcopy(ResList), CurrentScen_)
        finalRes.append({'Results': results, 'Scenario': CurrentScen_.name, 'SimRun': SimRun})
    SimRun+=1

with open('OntarioToU_Final','wb') as f:
    pickle.dump(finalRes, f)
