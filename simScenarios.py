from __future__ import division
from touEnv import ApplianceLoad



class Scenario:
    
    def __init__(self, ToU, Updater, Autoappliances, AutoTemp, name, seasonOrder, seasonWeeks, startWeek, infoCampaign):
        
        self.name = name
        self.startToU = ToU #ToU object containing peak, midpeak, offpeak hours, and prices 
        self.Updater = Updater
        self.auto = Autoappliances #Determine if we consider auto appliance control in this scenario
        self.autoTemp  = AutoTemp
        self.currentToU = ToU 
        self.currentSeason = seasonOrder[0] # 'summer' or 'winter', should start in May with summer
        self.seasonOrder = seasonOrder
        self.seasonWeeks = seasonWeeks #length of each season in weeks
        self.curWeeks = seasonWeeks[0]
        self.startWeek = startWeek # week of year when ToU scheme starts
        self.infoCampaign =  infoCampaign
        
    def estimateMonthlySavings(self, Appliance, WeekdayUsage):
        #Compare how much can be saved from using appliances during off peak periods
        #Appliance = appliance name, weekday usage = number of times used per week
        saved = ApplianceLoad[Appliance] * WeekdayUsage * (self.ToU.peakPrice - self.ToU.offPeakPrice)
        
        return saved
    
    def updateToU(self):
#         newUpdate = self.Updater.pop(1)
#         self.currentToU.peakPrice = round(self.startToU.peakPrice * newUpdate, 2)
#         self.currentToU.midPeakPrice = round(self.startToU.midPeakPrice * newUpdate, 2)
#         self.currentToU.offPeakPrice = round(self.startToU.offPeakPrice * newUpdate, 2)
        
        #Switch seasons
        curSeasonIdx =self.seasonOrder.index(self.currentSeason)
        #print curSeasonIdx
        if  curSeasonIdx == len(self.seasonOrder)-1:
            self.currentSeason = self.seasonOrder[0]
            self.curWeeks = self.seasonWeeks[0]
            
        else:
            self.currentSeason = self.seasonOrder[curSeasonIdx + 1]
            self.curWeeks = self.seasonWeeks[curSeasonIdx + 1]
            
        self.startWeek = self.startWeek + self.curWeeks
        if self.startWeek > 51:
            self.startWeek -=52