from __future__ import division
import csv
import pickle
import numpy as np
import datetime as dt
import math
import pylab as pl
import datetime

'''This contains environment variables that are used in the simulations
'''
AllAppliances = ['Dishwasher', 'Washing Machine', 'Cloth Dryer']
StartDate = 2016
Epochs = 10
ApplianceLoad = {'Dishwasher': 1, 'Washing Machine': 3.5, 'Cloth Dryer': 3.5} #kWh per use, http://www.txspc.com/documents/WattageAppliance.pdf
AwakeHours = np.asarray([0,0,0,0,0,0 ,1,1,1,1,1,1, 1,1,1,1,1,1, 1,1,1,1,1,1])
ACLoadSavings = 0.26 # kW savings for those who set their AC normally at <= 22 but will use auto temp at 25  
ACSeasons = ['summer','fall']
OffPeakElecPrice = 0.08 # $/kWh
SimYears = 1
HoursinWeek = 24*7
WeeksinMonth = 4
NumHouseholdsinON = 4887505
TotalSimRuns = 22
NumAgents = 206
#SummerMonths = [5,6,7,8,9,10]
#WinterMonths = [11,12,1,2,3,4]

# SurveyAgentKeys = ['Household Residents', 'School Age Children', 'Occupancy','Winter Bill','Summer Bill','Income',\
#                    'Education', 'Appliances', 'Bill Payment', 'ToU Knowledge', 'Perceived Peak/Off-peak', \
#                    'Perceived Savings', 'Off-peak Appliances', 'Washing Machine Savings', 'Cloth Dryer Savings',\
#                    'Dishwasher Savings', 'Washing Machine Ratio', 'Cloth Dryer Ratio', 'Dishwasher Ratio', \
#                    'Washing Machine Times', 'Cloth Dryer Times', 'Dishwasher Times','Washing Machine Frequency',\
#                    'Cloth Dryer Frequency', 'Dishwasher Frequency' , 'Social Influence', 'Auto Appliance', 'Auto Temp',\
#                    'Set Temp Savings']

# #Collect load data from file
# with open('AllLoads2', 'rb') as f:
#     AllLoads = pickle.load(f)       
# 
# 
# def writeToFile(count):
#     with open('load_' + str(count) +'.csv', 'a') as outcsv:   
#         #configure writer to write standard csv file
#         writer = csv.writer(outcsv, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL, lineterminator='\n')
#         for l in AllLoads[count]:
#             writer.writerow([l])
# 
# for i in range(100):
#     writeToFile(i)