from __future__ import division

# import matplotlib.pyplot as plt
import numpy as np

from importSurvey import surveyResponses
import random
import pickle
import agents
import math

def init(agentreplicas):
    AgentList = []
    NumPeople = len(surveyResponses)
    AllIDs= np.arange(0, NumPeople*agentreplicas)

    #Collect load data from file
    with open('AllLoads2', 'rb') as f:
        AllLoads = pickle.load(f)
    ID=0
    for i in range(NumPeople):
        k =0
        while k < agentreplicas:
            resp = surveyResponses[i]# Indep[i]

            PotentialFriends = np.select([AllIDs!=ID], [AllIDs])
            numFriends = int(math.ceil(random.betavariate(1,4) *10))

            socNet = random.sample(PotentialFriends, numFriends)
            
            load = random.choice(AllLoads)
            applianceList_ = []
            if 'washing_machine_laundry' in resp['Appliances']:
                applianceList_.append('Washing Machine')
            elif 'cloth_dryer_electric' in resp['Appliances']:
                applianceList_.append('Cloth Dryer')
            elif 'dishwasher' in resp['Appliances']:
                applianceList_.append('Dishwasher')
                
            applianceWeekday_ = dict()
            for appliance in applianceList_:
                if resp[appliance + ' Frequency'] is None:
                    #Assume weekly usage based on uniform random function
                    applianceWeekday_[appliance] = random.randint(1,6)
                else:
                    applianceWeekday_[appliance] = resp[appliance + ' Frequency']
            
            weekdayHours_ = dict()
            for appliance in applianceList_:
                weekdayHours_[appliance] = resp[appliance + ' Times']
            
            ToUInfluence_ = resp['Social Influence']
            willUseAuto_ = resp['Auto Appliance']
            willUseAutoTemp_ = resp['Auto Temp']
            ToUKnowledge_ = resp['ToU Knowledge']
            ToUKnowHow_ = 0
            Children = resp['School Age Children']
            
            OffPeakAppliances_ = []
            if 'washing_machine' in resp['Off-peak Appliances']:
                OffPeakAppliances_.append('Washing Machine')
            elif 'cloth_dryer' in resp['Off-peak Appliances']:
                OffPeakAppliances_.append('Cloth Dryer')
            elif 'dishwasher'  in resp['Off-peak Appliances']:
                OffPeakAppliances_.append('Dishwasher')
            
            resident = agents.Resident(load, applianceList_, applianceWeekday_, weekdayHours_, ToUInfluence_,\
                                       willUseAuto_, willUseAutoTemp_, ToUKnowledge_, ToUKnowHow_, socNet, Children,\
                                       OffPeakAppliances_)
            
            AgentList.append(resident)
            ID+=1
            k+=1

#     for ho in HomeOwnerList:
#         PotentialFriends = np.select([AllIDs!=ho.ID], [AllIDs])
#         numFriends = int(math.ceil(random.betavariate(1,5) *8))
#         socNet = random.sample(PotentialFriends, numFriends)
#         ho.network += socNet
#         for soc in socNet:
#             HomeOwnerList[soc].network.append(ho.ID)
# 
#     for ho in HomeOwnerList:
#         SocialSizes.append(len(ho.network))
        
#     plt.hist(SocialSizes)
#     plt.show()
    return AgentList