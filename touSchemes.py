from __future__ import division

'''All ToU scheme creation and change operations'''

class Scheme:
    
    def __init__(self, peakPrice, midPeakPrice, offPeakPrice, Seasons):
        #Hours 0-23 as used in datetime
        
        self.PeakHours = Seasons['peakHours'] #Array of peak hours (0,1 format)
        self.MidPeakHours = Seasons['midPeakHours']
        self.OffPeakHours = Seasons['offPeakHours']
        
        
        #Prices in $/kWh
        self.peakPrice = peakPrice
        self.midPeakPrice = midPeakPrice
        self.offPeakPrice = offPeakPrice
        
        